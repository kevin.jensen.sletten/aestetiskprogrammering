# ReadME miniX1
## Kevin Jensen

[Opgave. Det blinker meget!](https://kevin.jensen.sletten.gitlab.io/aestetiskprogrammering/MiniX_1/modul1sketch.html)

[Link til SourceCode](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_1/modul1sketch.js)


Jeg har fremstillet et kanvas i 500x500 pixels, dog har jeg undladt at farvebaggrunden med en 
konstant farve, ved ikke at skrive `’background’` under `’function setup’`, men i stedet placeret den under `’function draw’` for at programmet genkalder baggrunden for hver frame. Dette gør jeg for at fordi mit mål er at få en flakkende baggrund. Derefter har jeg bestemt farven, som et random parameter `background(random((255));`. På den måde vælger programmet en tilfældig farve i gråskalaen for hver frame, og skaber en flakkende baggrund. 
     
Som funktion har jeg valgt ellipsen, og havde et ønske om at lave en form for gradiering, så jeg skulle have cirkler inde i cirkler og farver en farve gradiering. Først lavede jeg koden til gradieringen, ved at bruge `’lerpColor’`. Denne funktion fungere ved at man finder to farver der ”blandes” for at finde en farve der ligger på skalaen imellem dem. For at finde blandingsforholdet bruges et `’amt’` parameter der går fra 0 til 1, hvor 0 er den ene farve og 1 er den anden farve. Blandingsforholdet ligger så i koden ved at skrive et parameter der ligger herimellem, f.eks. 0.20. I min kode ser det således ud:
  ```
  let from = color(51,51,0);
  let to = color(204,51,0);
  let interA = lerpColor (from, to, 0.16);
  let interB = lerpColor (from, to, 0.32);
  let interC = lerpColor (from, to, 0.48);
  let interD = lerpColor (from, to, 0.64);
  let interE = lerpColor (from, to, 0.80);
```

Jeg har også haft et ønske om at have inkludere noget bruger interaktion. Det løste jeg ved at tilføje en funktion der hedder `’if (mouseIsPressed===true)’`, som siger at hvis det er rigtigt (true) at der trykkes på musen, så sker den kode der er skrevet nedenunder. Hvis der trykkes på musen efter koden, er sat i gang, altså `mouseIsPressed===true` så læser programmet blandt andet:
```
ellipseMode(CENTER);
fill(from);
ellipse(250,250,100,100);
ellipseMode(CENTER);
fill(interA);
ellipse(250,250,85.72,85.72);
```

`ellipseMode(CENTER)` fortæller egentlig bare hvilke koordinater ellipsen skal tegnes ud fra. I dette tilfælde skal ellipsens center tegnes ud fra x = 250 og y = 250, altså midt i billedet. Dette ville funktionen også gøre som standard, så `ellipseMode(CENTER)`, set i bakspejlet, er egentlig en overflødig linje. `fill` bestemmer ellipsens farve, og i dette tilfælde har jeg fortalt programmet at `from = color(51,51,0)` atså er `from` det samme, som farven mørkegrøn. Det samme er gældende for `to`, som i dette tilfælde er det samme som i koral farve. `ellipse(250,250,100,100);` kalder funktionen ellipse, hvor dens center skal være og hvor stor den skal være. I den næsten ellipse er `fill = interA`. I dette tilfælde har jeg fortalt programmet at, dette er den `lerpColor` (den gradient/blanding) jeg lavede tidligere i koden. 


Koden slutter med en `else statement`, som fortæller at hvis `mouseIsPressed` ikke er `true`, hvis der ikke trykkes på musen, så skal nedenstående kode forekomme:
```
else {
stroke(0);
ellipseMode(RADIUS);
fill(0);
ellipse(250,250,50,50);
}
```
Dette vil sige at hvis der ikke trykkes på musen vises der en ellipse med placeringen 250 i både x og y aksen, den har en `stroke` i sort og ved at bruge `RADIUS`, tegnes ellipsen stadig fra 250,250, men bruger 50,50 til at definere halvdelen af ellipsen højde og bredde, altså når `RADIUS` bruges og der skrives 50,50 er det egentlig det samme, som at skrive 100,100 i forhold til størrelsen. 


Det har været sjovt at arbejde med kodning på denne måde, det er forholdsvist let at gå til og det fordrer til leg og at eksperimentere. I starten virker det uoverskueligt og man ved ikke hvor man skal starte, men efter at man får læst referencerne igennem bliver mulighederne synlige. Man prøver lidt at sætte noget sammen, og hvis det ikke virker prøver man at ændre lidt her og der, og hvis man er heldig eller dygtig, så virker det til sidst. Det er klart et felt jeg føler i høj grad er præget af learning by doing. Det er skønt med mere eller mindre instant gradification, man skriver noget og med det samme, såfremt det virker, kan man se det på skærmen når man ”går live”. Det er enormt tilfredsstillende. 


Jeg føler at kodning er lidt, som hvis man skulle snakke og skrive fonetisk, det ser vanvittigt ud, men er egentlig ret simpelt hvis man forstår syntaksen. Det er meget konkret, computeren skal have alle ting med, hvem, hvad, hvor, hvordan og hvornår. Kodning i dette tilfælde er anderledes fra at skrive og læse en almindelig tekst, fordi man arbejder baglæns. I stedet for at have haft en plan og udføre den gradvist i en fremadgående retning, så at sige, til at indsætte en bid kode, og bagefter ind og analysere, samt prøve at forstå hvad gør hvad. 


Det at programmere giver mig en større forståelse for det der ligger bagved den software jeg bruger dagligt. At jeg trykker på en knap og at der så sker noget på skærmen, er ikke tilfældigt, det er ikke fordi designet er som det er, men fordi der er en person der har siddet og kodet det til at sådan skal det være. Det vi har læst indtil videre, er ikke fordi det har bragt en masse nyt på banen, men det får alligevel en til at reflektere over hvor meget ’kode’ vi interagere med hver dag, som vi ikke kan læse eller forstå, og hvad har det af betydning for os.      


<img src="miniX1_screenshot.PNG" width="200" height="200"> <img src="miniX1_screenshot2.PNG" width="200" height="200">

