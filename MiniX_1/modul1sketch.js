function setup() {
  // put setup code here
createCanvas(500,500);
print("hello world!");
}

function draw() {
  // put drawing code here
  stroke(255);
  background(random(255));
  let from = color(51,51,0);
  let to = color(204,51,0);
  let interA = lerpColor (from, to, 0.16);
  let interB = lerpColor (from, to, 0.32);
  let interC = lerpColor (from, to, 0.48);
  let interD = lerpColor (from, to, 0.64);
  let interE = lerpColor (from, to, 0.80);
  if (mouseIsPressed===true) {
    ellipseMode(CENTER);
    fill(from);
    ellipse(250,250,100,100);
    ellipseMode(CENTER);
    fill(interA);
    ellipse(250,250,85.72,85.72);
    ellipseMode(CENTER);
    fill(interB);
    ellipse(250,250,71.44,71.44);
    ellipseMode(CENTER);
    fill(interC);
    ellipse(250,250,57.16,57.16);
    ellipseMode(CENTER);
    fill(interD);
    ellipse(250,250,42.88,42.88);
    ellipseMode(CENTER);
    fill(interE);
    ellipse(250,250,28.63,28.62);
    ellipseMode(CENTER);
    fill(to);
    ellipse(250,250,14.28,14.28);
  } else {  
    stroke(0);
    ellipseMode(RADIUS);
    fill(0);
    ellipse(250,250,50,50);
  }

}
