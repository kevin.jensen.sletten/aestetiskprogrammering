# MiniX10: Machine unlearning

[Programmet](https://kevin.jensen.sletten.gitlab.io/aestetiskprogrammering/MiniX_10/index.html)


[Koden til programmet](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_10/sketch.js)


[Buster](https://gitlab.com/bustersiem/aestetisk-programmering),[Mathias](https://gitlab.com/MathiasSund/aestetisk-programmering),[Christoffer](https://gitlab.com/Thundt/aestetiskprgrammering),[Kevin](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering)



## ReadMe
#### SoundClassifier (https://learn.ml5js.org/#/reference/sound-classifier)
```
const options = { probabilityThreshold: 0.7 };
const classifier = ml5.soundClassifier('SpeechCommands18w', options, modelReady);

function modelReady() {
  classifier.classify(gotResult);
}

function gotResult(error, result) {
  if (error) {
    console.log(error);
    return;
  }
  console.log(result);
}
```

Vi har valgt at skrive om **SoundClassifier**, da vi på baggrund af research har fundet ud af at dette felt indeholder problematikker i forhold til racial bias, samt forskel i sprog, med hensyn til dialekter og native speakers kontra second language speakers. Speech recognition AI’s, bliver større og større, det ses i forhold til smart homes, hvor funktioner i hjemmet kan kontrolleres ved hjælp af en app eller en stemme, som ved Alexa, Siri eller Google Home. Samme stemme genkendelse har også gjort sit indryk i biler, især i elbiler, hvor bilens funktioner styres af en computer, der respondere på ens stemme. På denne måde bliver stemmestyring en af de mest gængse måder at mennesker direkte og bevidst interagere med AI’s på. Med dette i mente bliver det interessant, hvis ikke vigtigt, at have fokus på hvordan denne stemme styrings AI bliver trænet, på hvilket sprog, på hvilke dialekter, hvilke accenter, og hvilke ord der indgår.


Vi har lavet koden i Visual Studio Code for at få et indblik i kodens funktionalitet, samtidig var vi nysgerrige på `{ probabilityThreshold: 0.7 }` og dets betydning for udfaldet i koden. Jo lavere en værdi denne konstant besidder, jo mere tilbøjelig er programmet til at fortolke og acceptere dit input, modsat jo højere værdien er, jo mere skal inputtet minde om det træningsmateriale, som programmet er trænet på. Grunden til at dette fangede vores opmærksomhed var for at teste om programmet kunne forstå vores accent og hvor høj vores confidence score ville være. `Confidence` er iboende ml5 hvis output giver en procentværdi af “korrekthed”, dette implementerede vi ved hjælp af Daniel Schiffman, og i forhold til vores input, med accent, fik vi en høj confidence score alligevel. 


Som vi tidligere har skrevet om, synes vi specielt at `options` variablen (der indeholder `probabilityThreshold`), fordi den værdisætter og vurdere hvad der er "rigtigt" - som er et meget kontroversielt emne i et samfundsrelateret aspekt.


For at teste algoritmen ville vi udsætte den for forskellige accenter og dialekter, for at undersøge hvor høj en confidence score de ville få. AI’en er ikke trænet på andre sprog end amerikansk engelsk, så det ville ikke give mening at tale et andet sprog til den, men i forhold til den benyttede artikel, er det åbenlyst at accent spiller en væsentlig rolle i voice recognition software. Vi har, som gruppe, allerede testet om programmet vil anerkende vores egen accent, og her får vi en høj confidence score, så det er tilsyneladende ikke noget problem, men det kunne være interessant at teste med accent fra et ikke germansk opstået sprog, som kinesisk eller arabisk. Desuden testede vi programmet ved at sige ord, der på sin vis minder om de i forvejen indøvede ord, som up - Oppenheimer, four - foreclosure. I disse tilfælde reagerede programmet også med et confidence response, altså reagerede på ordet på trods af at det kun indeholdte en stavelse af ordet. Dette kaldes en **false positive**. Denne false positive lader det sig også til at programmet henvises til, da ordet “up” fra tid til anden, dukker op med en høj confidence score, selvom ordet ikke er sagt. Dette tyder også på at programmet fungere på baggrund af keyword spotting.


Før forelæsningen i mandags kendte gruppen ikke meget til til ml5-biblioteket, og dermed vidste vi ikke at man med `ml5` kunne kalde på en *pre-trained* algoritme som f.eks. `.speechCommand`. For at tilgå den trænede model bruges `”SpeechCommands18w”` for at specificere hvilken trænet model der skal bruges og hvad den kan - lige præcis denne model indeholder lige netop 18 forskellige ord som algoritmen er trænet til at lytte efter. Ud over indholdet i `classifier`-variablen, var funktionen med `probabilityThreshold` også ny for os - vi synes at det giver god mening at have en variabel, der bestemmer hvornår at computeren skal godtage de input, der kommer fra brugeren. Den sidste syntaks som vi ikke helt kunne genkende var `classify` som på en eller anden måde klassificerer (for eksempel ved hjælp af probablity) lyden fra mikrofonen i live tid.


Det eksempel på kode som vores gruppe har valgt at undersøge har en klar relation med Voice Assistance AI, da vores eksempel er en Speech Recognition algoritme der giver et indblik i hvordan stemmegenkendelse fungerer. Den kode vi har undersøgt giver altså et lille indblik i hvordan en Voice Assistance AI er trænet og hvilke problematikker der kan opstå når Voice Assistance AI skal genkende stemmer. Det kan være når en person med anden dialekt eller accent skal bruge et produkt med stemmegenkendelse f.eks. Amazon Alexa, og derfor har problemer med at få AI’en til at forstå hvad der bliver sagt. I artiklen fra Frontiers in Communication undersøgte de forskellen på brugen af Alexa mellem mennesker med engelsk som modersmål og mennesker med koreansk som modersmål, med et fokus på specifikke vokaler som ikke eksisterer på koreansk. Her fandt de frem til at når de med engelsk modersmål brugte Alexa, var der en 98 % succesrate, mens den kun var på 55 % for dem med koreansk modersmål. Det interessante de finder frem til er at når de med koreansk modersmål fik en error, ville de ændre måden de udtalte ordet på, og derfor tilpasser sig AI’ens regelsæt. 


Hvis vi skulle tænke over hvordan algoritmer kan diskriminere og være mere eller mindre ekskluderende, så kunne det være interessant at kigge på hvem der  egentlig forbedre sig når det kommer til forskelle i accenter? Skal det være voice assistenten, der bør trænes bedre til at kunne forstå flere forskellige måder at tale engelsk på, eller er det brugerne der skal trænes i en mere “korrekt” engelsk accent?


<img src="minix10.png" width="439" height="260"> 
<img src="minix10_1.png" width="374" height="305">




### Referenceliste
ml5 - A friendly machine learning library for the web. https://learn.ml5js.org/#/reference/sound-classifier.


Song, J. Y., Pycha, A., & Culleton, T. (2022). Interactions between voice-activated AI assistants and human speakers and their implications for second-language acquisition. Frontiers in Communication, 7. https://doi.org/10.3389/fcomm.2022.995475.


The Coding Train. (2019, June 21). ml5.js: Sound Classification [Video]. YouTube. https://www.youtube.com/watch?v=cO4UP2dX944.


Peter Warden (2018). Speech Commands: A Dataset for Limited-Vocabulary Speech Recognition. Google Brain. Mountain View, California https://arxiv.org/pdf/1804.03209.pdf
