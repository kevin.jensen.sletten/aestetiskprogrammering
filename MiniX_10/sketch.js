      // Your code will go here
      // open up your console - if everything loaded properly you should see the latest ml5 version
      console.log('ml5 version:', ml5.version);

      let soundClassifier;
      let resultP;
      function preload() {
        let options ={ probabilityThreshold: 0.95};
        soundClassifier = ml5.soundClassifier('SpeechCommands18w', options);
      }

      function setup() {
        createCanvas(400, 400);
        resultP = createP('waiting...');
        soundClassifier.classify(gotResults);
      }

      function gotResults(error, results) {
        if (error) {
            console.log('something went wrong');
            console.log(error);
        }
        resultP.html(`${results[0].label} : ${results[0].confidence}`);
      }