// arrays og variabler
let colorLight;
let throbberText = ["Here we go!", "Hang on", "Almost there", "About 5 more seconds", "Maybe 7", "...", "How was your day?"];
let hastighed = [1, 5, 10, 15, 20];
let vinkel = 0;
let translateX;
let translateY;


function setup(){

    createCanvas(windowWidth,windowHeight);
    /* et forsøg på at gøre translate til en kode der løber én gang,
    i stedet for at koden kaldes i et loop under 'draw'. Virker ikke */
    translateX = width/2;
    translateY = height/2;
    // Ændre vinkel beregninger fra radianer til grader
    angleMode(DEGREES);
    frameRate(60);

}    


function draw(){

    // Kalder min translate, kunne bare have hedder width/2, height/2
    translate(translateX,translateY);
    // baggrund med alpha værdi
    background(250,240,250,30);
    /* kaldt min egen function, nemmere at læse når for andre 
    når de forskellige elementer har sin egen "plads" */
    drawThrobber();

}

    // egen function til at styre throbber
function drawThrobber(){

    // laver variabel for millisekunder
    let m = millis();
    /* laver variabel for interval, omdanner 5000 millisekunder til 5 sekunder, 
    samt laver det om til en integer, så der ikke er decimaler med */
    let interval = int(m / 5000);
    // laver variabel for farve skift baseret intervallet (5 sek)
    let colorInterval = interval;
    // laver array over throbberens skiftende farve koder
    let colorLight = [color(40), color(80), color(120), color(160), color(200)];
    // laver variabel for throbberens skiftende hastighed baseret på intervallet
    let hastighedInterval = interval;

    /* gemmer den indeholdende source code, da rotationen ellers 
    havde indflydelse på teksten, og teksten roterede med throbberen */
    push();

    /* Hvis der går mere end 5 sekunder, skift farve i forhold til 
    colorLight arrayets længde */
    if(interval > colorLight.length - 1){
        colorInterval = colorLight.length - 1;
    }

    /* Hvis der går mere end 5 sekunder, ændre hastighed 
    baseret på hastigheds arrayet */
    if(interval > hastighed.length - 1){
        hastighedInterval = hastighed.length - 1;
    }
    
    // Roter i forhold til if statements løbende i forhold til hastigheds array
    rotate(vinkel += hastighed[hastighedInterval]);
    noStroke();
    // Cirklens farve ændres i forhold til colorLight arrayet, i forhold til colorIntervallet
    fill(colorLight[colorInterval]);
    circle (15, 15, 20, 20);
    

    /* Oprindelige kode, som blev skiftet med hastighed[hastighedInterval]
    
    if (m < 5000){

        vinkel += 1;

    } else if (m > 5000 && m < 10000){
        
        vinkel += 5;
    } else if (m > 10000 && m < 15000){

        vinkel += 10;
    } else if (m > 15000 && m < 20000){

        vinkel += 15;
    } else if (m > 20000){

        vinkel += 20;
    } */
    pop();
    // Kalder min egen function 'textThrobber' som indeholder tekst
    textThrobber(interval);
    
}

// funktionen jeg har lavet tager værdien på 5 sekunder fra intervallet jeg har lavet
function textThrobber(interval){
    
    // Laver en variabel der skal skifte tekst baseret på de 5 sekunder
    let textInterval = interval;
    
    // Hvis der går mere end 5 sekunder, skift tekst baseret på tekst arrayet
    if(interval > throbberText.length - 1){
        textInterval = throbberText.length - 1;
    }
    fill(50);
    textSize(10);
    textFont('Roboto');
    
    // Laver et tekstbox og centrere teksten i forhold til tekstens længde
    textAlign(CENTER);
    text(throbberText[textInterval], -100, -45, 200);
}

function windowResized(){
    resizeCanvas(windowWidth, windowHeight);
}