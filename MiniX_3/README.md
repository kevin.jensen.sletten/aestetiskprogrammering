# miniX3 README


## Designing a Throbber - Infinity loops && temporalitet


Med min [RunMe](https://kevin.jensen.sletten.gitlab.io/aestetiskprogrammering/MiniX_3/minix3.html) denne uge har jeg taget et kig tilbage ([sourceCode](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_3/minix3.js)). Dengang jeg var ung... Vi diskuterede vores oplevelse eller opfattelse af throbbers i undervisningen en mandag. Her oplevede jeg at der var mange der var mere eller mindre negative overfor throbbers og deres funktion og fortalte, at så snart de så en throbber, på en hjemmeside, youtube eller netflix så reloadede de siden. Min personlige oplevelse af throbbers er at de er en kæmpe hjælp, for jeg mindes en tid hvor der ikke var noget decideret grafisk udtryk for at computeren tænkte. På nogle hjemmesider mindes jeg at man kunne se skiftende computer relevante tekst segmenter ville skifte nede i venstre hjørne af browser vinduet, hvilket gav en indikation af at computeren arbejdede. Desuden er der selvfølgelig også det aspekt at computere er blevet vanvigttige hurtige, i forhold til hvad de var en gang, så måske har jeg en tilbøjelighed til at være lidt mere tålmodig? Det er i hvert fald det udgangspunkt jeg har taget i min miniX opgave, og prøvet at putte lidt humor ind i det også. 


I forhold til temporaliteten i koden har jeg inddraget tid `let m = millis();` Denne linje lader mig kalde en variabel der tæller millisekunder. Millisekunder tæller decimaler og er en `float`, jeg havde et ønske at regne med hele tal, da jeg ville have min kode skulle skifte hvert 5 sekund lige ud. Dette løser jeg ved at lave en variable der hedder `let interval = int(m / 5000);`. Her dividere jeg mine millisekunder med 5000 og får 5, så min int eller integer (hele tal, uden decimaler) bliver 5 lige ud. Derefter laver jeg variabler baseret på intervallet på 5 sekunder og siger:

```
let hastighedsInterval = interval
let colorInterval = interval
```
Nu kan jeg kalde at farverne og hastigheden skifter hvert femte sekund ved at bruge `if statements`:
```
    if(interval > colorLight.length - 1){
        colorInterval = colorLight.length - 1;
    }
```
Og "programmet" ved hvor hvornår hver farve og hastighed skal komme i forhold til tid der er gået og datatypens plads i arrayet ved:
```
rotate(vinkel += hastighed[hastighedInterval]);
fill(colorLight[colorInterval]);
```
Derefter ville jeg gerne have er der skulle være noget tekst der kun give en yderligere indikation af at computeren arbejder, uden at det skulle blive en teknisk forklaring af de individuelle processor der foregår bag skærmen. Problemet var dog at da jeg lavede teksten drejede den rundt sammen med throbberen, hvilket ikke var meningen. Jeg prøvede så med en henholdvis `pop(); && push();`funktion, men så havde teksten ikke det skift i forhold til de fem sekunder, som jeg ønskede. Så med lidt hjælp, fik jeg lavet en ny funktion til teksten, og tog interval værdien fra det tidligere stykke kode:
```
function textThrobber(interval){
}
```
Ved at skrive "interval" i parentesserne, hvilket tager interval funktionen eller værdien med ned til nye funktion. Dermed kan jeg skifte text baseret på de fem sekunder, som tidligere er defineret, på samme måde som jeg ændre hastighed og farve, men hvor teksten står fast:
```
if(interval > throbberText.length - 1){
        textInterval = throbberText.length - 1;

text(throbberText[textInterval], -100, -45, 200);
```
Oprindeligt var planen at lave throbberen til et lille spil, lidt i stil med YouTubes tidligere throbber der blev til et spil snake. Planen var at de skulle have været et casino lignende spil, som en enarmet tyveknægt. Denne endelige throbber betød dog at jeg i højere grad kunne "kommentere" på temporaliteten, ved at benytte tid direkte i koden, samt flere visuelle virkemidler til at synliggøre tiden der går.


<img src="Throbber_5_more_seconds.PNG" width="400" height="300"> <img src="Throbber_hang_on.PNG" width="400" height="300">
