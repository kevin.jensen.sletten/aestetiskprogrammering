# Hej og velkommen
Dette er min første repository til faget ÆP

## Header 2
### Header 3
###### Header 6

**Dette er en fed tekst** modsat den normale som ser sådan her ud

_Sådan her ser skråskrift ud_

> Dette er et citat

Dette er en funktion der kan lave en `Codestick`

```
Dette er en kodeblok
den kan være
flere linjer lang
```

- liste 1
    - liste 1 liste
- liste 2

[Sådan laver du et hyperlink](https://da.wikipedia.org/wiki/Forside)

![](https://media.giphy.com/media/GHl7ywzbuT8KQ/giphy.gif)
