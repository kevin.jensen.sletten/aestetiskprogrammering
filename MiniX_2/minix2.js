  // Laver en Global variabel og deklarere den
let slider;

let p1 = { x: 320, y: 360};
let p2 = { x: 372.5, y: 360};
let p3 = { x: 425, y: 360};
let p4 = { x: 477.5, y: 360};
let p5 = { x: 530, y: 360};

let emotionSlider;


let b1 = { x: 350, y: 250};
let b2 = { x: 415, y: 250};

let b3 = { x: 435, y: 250};
let b4 = { x: 500, y: 250};

let browSlider;

let eyeSlider;

function setup() {
  // Canvas størrelse
createCanvas (1500,600);
background (255);
  // HSB står for hue, saturation && brightness
colorMode(HSB);
  /* Tildeler min variabel en funktion, "createSlider",
  samt giver den minimum farve værdi (0), maximum farveværdi(360),
  en default farveværdi (0, hvor den starter på farvecirklen), hvor mange skridt der skal
  tages for at komme hele hue farvecirklen rundt (40 = 360/9 = 9 skridt) */
slider = createSlider(0, 350, 0, 40);
  // Slider position 10x, 10y
slider.position(10, 10);
  // Slider stil = bredden = 80px
slider.style('width', '80px');
  /* Slider til styring af munden. 320 = minimum y koordinat.
  400 = maximum y koordinat, 360 = default placering på slider*/
emotionSlider = createSlider (320, 400, 360, .1);
emotionSlider.position(10,40);
emotionSlider.style('width','80px');


browSlider = createSlider (220, 270, 245, .1);
browSlider.position(10,70);
browSlider.style('width','80px');


eyeSlider = createSlider (0,350,0,40),
eyeSlider.position(10,100);
eyeSlider.style('width','80px');

textSize(15);
}

function draw() {

  // Lokal variabel = val. Val giver værdi efter slider
  let val = slider.value();
  /* val definere 'Hue' (farven) i cirklen efter sliderens placering,
  100 er saturation (farvemætningen), 50 er brightness.*/
  fill(val,100,100,1);
  // Laver en cirkel med placering og størrelse
  circle(width/3.5,height/2,250,250);
  // Som før, lokal variabel, giver y koordinat efter slider
  let eVal = emotionSlider.value();
  // Custom punkter tegnet på baggrund af global variabel koordinator

  
  noFill();
  stroke(0);
  strokeWeight(5);
  beginShape();
  curveVertex(p1.x, p1.y);
  curveVertex(p2.x, p2.y);
  curveVertex(p3.x, eVal);
  curveVertex(p4.x, p3.y);
  curveVertex(p5.x, p5.y);
  endShape();

  // Definere bVal værdi på baggrund af slider
  let bVal = browSlider.value();

  // Øjenbryn
  beginShape(LINES);
  curveVertex(b1.x, b1.y);
  curveVertex(b2.x, bVal);
  endShape();

  // Øjenbryn
  beginShape(LINES);
  curveVertex(b3.x, bVal);
  curveVertex(b4.x, b4.y);
  endShape();

  // Øjebolle
  fill(255);
  ellipse(390,290,50,50);
  ellipse(460,290,50,50);
  
  // Slider text
  noStroke();
  fill(0);
  text('Smiley color', slider.x * 2 + slider.width, 27);
  text('Mouth', emotionSlider.x * 2 + emotionSlider.width, 57);
  text('Brows', browSlider.x * 2 + browSlider.width, 87);
  text('Eye color', eyeSlider.x * 2 + eyeSlider.width, 117);

  // henter functionen jeg selv har defineret
  drawEyes();

}

  // Definere ny function for at adskille iris fra resten af tegningen
function drawEyes() {
  // Iris. Definere iVal værdien på baggrund af slider
  let iVal = eyeSlider.value();
  // Farver iris på baggrund af slider
  fill(iVal,100,iVal,1);
  stroke(0);
  strokeWeight(2);
  ellipse(395,295,25,25);
  ellipse(455,295,25,25);

  // Pupil
  fill(0);
  noStroke();
  circle(395,295,15,15);
  circle(455,295,15,15);

  // Iris
  fill(255);
  noStroke();
  ellipse(390,288,5,15);
  ellipse(450,288,5,15);





}




