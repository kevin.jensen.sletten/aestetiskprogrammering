# miniX2 readMe


Dette er min [Emoji](https://kevin.jensen.sletten.gitlab.io/aestetiskprogrammering/MiniX_2/minix2.html)


Dette er min [SourceCode](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_2/minix2.js)


Indledende til denne uges opgave læste og hørte vi om emojis og deres betydning i samfundet i forhold til repræsentation og stereotyping. Det har handlet om farven på emojis, deres forskellige udtryk med tilhørende kulturelle fortolkninger, stereotyper og køn. Mit ønske har i det henseende været at lave en emoji, som både ville kunne repræsentere det rette udtryk, men samtidig undlade at repræsentere stereotyper, og hudfarve. 
Jeg startede ud i bedste emoji stil med at lave en cirkel `circle(width/3.5,height/2,250,250);`. Så langt så godt. Derefter var jeg ret sikker på jeg ville have noget med en slider `let slider;` og farve. Slideren skulle så kaldes i programmet, samt tilhørende funktion (farve):
```
colorMode(HSB)
slider = createSlider(0, 350, 0, 40);
slider.position(10,10);
slider.style(’width’, ’80px’);
```
Nu har jeg en slider, der har en minimumværdi af 0 og en maximumværdi på 350, Hue, saturation og brightness er en farvecirkel. For at jeg kan bruge sliderens værdi, skal jeg benytte dens værdi. Jeg ønskede at man skulle kunne skifte farve på cirklen/emojien, dog ikke til hudfarver, men regnbuefarver. Grunden til dette skyldes at jeg ikke føler at en hudfarve er nødvendig for at kunne illustrere en følelse, samtidig kan forskellige farver også udtrykke følelser. Jeg kalder sliderens værdi for `let val = slider.value();` og benytter denne værdi til at styre ´Hue´ (HSB) i `fill(val,100,100,1);`. Nu kan jeg styre cirklens farve. Det er svært at udtrykke følelser uden en mund. Jeg forsøgte mig med `arc()`, men kunne ikke helt få det til at virke som jeg ville. Nu havde jeg fået en slider til at fungere, så måske kunne jeg bruge sådan en igen, men denne gang til at bestemme mundens kurve. Til denne skulle jeg skabe min egen form, eller det vil sige en linje, da jeg mente det ville være nemmere at manipulere med en slider. Derfor benyttede jeg mig af `beginShape();` og `endShape();`. Til at lave sin egen form skal man bruge et x-koordinat og et y-koordinat for hvert et punkt i formen. Da jeg ville lave en linje skulle punkterne agere som manipulationspunkter, så på trods af det ”bare” var en linje, skulle den have mere end to punkter. Jeg lavede min egen variabel i forhold til koordinaterne da jeg mente at det ville gøre det mere overskueligt, er ikke sikker på det nødvendigvis lykkedes. De blev dog kaldt således: 
```
let p1 = { x: 320, y: 360};
let p2 = { x:372.5, y: 360};
osv …
```
Disse blev så ført ind i `beginShape();` under `curveVertex(p1.x, p1.y)`. Efter at jeg havde fået fremstillet en linke, skulle denne linje så kunne laves til et smil, eller en sur mund ved hjælp af en slider. Igen skulle der `defineres` en slider `let emotionSlider;`. Det var dog ikke en farvekode/værdi den skulle styre, men koordinator, og dette var ikke nemt. Efter lang tid med referencelister og youtube videoer, på grænsen til at give op, og med hjælp fra en langt mere fagkyndig, lykkedes det endelig at få det konceptuelt til at virke. Jeg fjernede en `vertex` så der var fem i stedet for seks. Benyttede sliderværdien `let eVal=emotionSlider.value();` til den midterste `vertex`’s y-koordinat og definerede min og max værdi, som minimum og maximum placering i y-aksen. Det virker irriterende simpelt nu hvor jeg skriver det, men det var ved at blive smileyens endeligt i situationen.
Følelsesmæssige udtryk sidder også i øjenbrynene, så det var også vigtige at have med, samt muligheden for at customise disse. Her gjaldt det samme princip som med munden `let bVal = browSlider.value();`, dog med en simplere form, der kun bestod af to punkter. 
```
beginShape(LINES);
curveVertex(b1.x,b1.y);
curveVertex(b2.x,bVal);
endShape();
```  
Til smileyens Iris og pupil valgte jeg at definere en ny `function`, da jeg ikke kunne lave om på `strokeWeight` uden at hele smileyen fik en tykkere stroke. Denne kaldte jeg `function drawEyes() { }, og kaldte den under `function draw() { }` så programmet vidste at den skulle læse min function, da den ikke er en del af P5js biblioteket. Her lavede jeg også endnu en slider, så brugeren ville kunne ændre øjenfarven. 


>>inb4 opgaven lød på to smileyer, jeg vil mene at gennem muligheden for at customize smileyen har lavet væsentligt flere.


Gennem udarbejdelsen af denne smiley, på baggrund af tekst og video, har jeg tænkt en del over hvordan jeg kunne få denne smiley til favne bredt og uden at have for mange politiske værdier med i bagagen. Dette er forsøgt ved at undlade hudfarve. Jeg føler lidt at Femke Snelting pointere dette med at få megen specifik repræsentation ville være håbløst, i forhold til alle de muligheder der skulle være tilgængelige. I takt med at vores kommunikationsmidler er blevet smartere og hurtigere, og at vi nu har sms’er og diverse besked applikationer, fordrer også at vi formidler os langt mere kortfattet. Der er dermed ikke altid plads til den rette mængde tillægsord for at videregive den korrekte følelse, og det er her Emojis kommer ind i billedet. De kan i højere grad illustrere den følelse man gerne vil videregive, uden alle de nødvendige tillægsord. Emojis formidler følelser, de skal ikke repræsentere Lars Karlsen fra Jyllingevej 142 i Værløse. Derfor mener jeg også at hudfarve på emojis er irrelevant. At følelser kan udtrykkes forskelligt på tværs af samfund og kultur kan jeg godt følge og derfor har jeg gjort det muligt at ændre smileyens udtryk. 


Jeg føler ikke at min emoji formår at kommenterer på kønsproblematik/ulighed, og ved at den ikke gør det, betyder det at den alligevel gør. Hvordan jeg skulle have løst dette aspekt, eller hvordan min emoji skulle have kommenteret på det, kan jeg ikke se. Føler ikke at ændre hårlængde er tilstrækkeligt. 

<img src="Emoji.PNG" width="400" height="300"> ![](https://media.giphy.com/media/L3ERvA6jWCd0qO4NdX/giphy.gif)
