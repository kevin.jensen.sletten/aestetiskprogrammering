class Particles {
    constructor() {
        //Position
        //Skaber en ny p5.vector der indeholder magnitude og retning.
        //Bruges til at beskrive en pos, hastighed og acceleration.
        this.pos = createVector(random(width),random(height));
        //Velocity = speed of motion, action, or operation
        this.vel = createVector(0,0);
        /*Acceleration = Acceleration is the rate of change
        of the velocity of an object with respect to time.*/
        this.acc = createVector(0,0);
        this.maxSpeed = 2;
        //kopiere tidligere position
        this.prevPos = this.pos.copy();
    }

    update() {
        //Update dens hastighed baseret på den acceleration
        this.vel.add(this.acc);
        //max hastighed på partiklerne
        this.vel.limit(this.maxSpeed);
        //Update dens position baseret på dens hastighed
        this.pos.add(this.vel);
        //Update. multiply dens acceleration med værdien 0 i både x, y og z
        this.acc.mult(0);

    }

    /*Baseret på partiklets x, y position, scaler det til et grid af cols og rows,
    find den tætteste vector i det én dimensionelle array (x + y * cols), brug den
    vector og apply dens force*/
    follow(vectors) {
        let x = floor(this.pos.x / scale);
        let y = floor(this.pos.y / scale);
        let index = x + y * cols;
        let force = vectors[index];
        this.applyForce(force);
    }

    applyForce(force) {
        //akkumuler "force" til acceleration 
        this.acc.add(force);
    }

    show() {
        //Dette var et forsøg på at bruge amplitude til strokeweight, og virker
        //let volume = mic.getLevel();
        //map(incoming value to be converted, lower bound current range, upper bound current range, lower bound target value, upper bound target value);
        //let mappedVolume = map(volume,0,1,0,500);
        //Particlets udseende
        
        //Bruges til lyd værdier
        stroke(255, 5);
        strokeWeight(1);//(mappedVolume);
        line(this.pos.x, this.pos.y, this.prevPos.x, this.prevPos.y);
        this.updatePrev();
    }

    updatePrev() {
        this.prevPos.x = this.pos.x
        this.prevPos.y = this.pos.y
    }
    //Hvis partiklen kommer ud til kanten, sæt den over i modsatte side, baseret på dens tidligere placering
    edges() {
        if (this.pos.x > width) {
            this.pos.x = 0;
            this.updatePrev();
        }
        
        if (this.pos.x < 0) {
            this.pos.x = width;
            this.updatePrev();
        }
        
        if (this.pos.y > height) {
            this.pos.y = 0;
            this.updatePrev();
        }
        
        if (this.pos.y < 0) {
            this.pos.y = height;
            this.updatePrev();
        }

    }
}