//Flow field

//MUST HAVES
//Der skal være et grid [✔]
//Der skal bruges noise [✔]
//Programmet skal repræsentere noget organisk [✔]
//Skal indeholde minimum 1 for-loop [✔]
//Der skal være to conditional statements [✔]

//NICE TO HAVE
//Det er en cirkel [✔]
//Det ligner "hår" [✔]

let particles = [];
let flowField;
let inc = 0.1;
let scale = 10;
let cols, rows;
let zoff = 0;

function setup() {
    createCanvas(600, 600);
    //Floor runder ned til nærmeste hele tal, en vector for hver 10px
    cols = floor (width / scale);
    rows = floor (height / scale);
    flowField = new Array(cols * rows);

    //Til lyd input bruges ca 50
    for (let i = 0; i < 500; i++) {
        particles[i] = new Particles();
    }
    
    mic = new p5.AudioIn();
    mic.start();
    
    background(0);
}

function draw() {
    let yoff = 0;
    //getLevel afgiver en enkelt amplitude reading når den er kaldt (hvilket er heletiden i draw)
    //Amplitude is the magnitude of vibration. Sound is vibration, so its amplitude is is closely related to volume / loudness.
    let volume = mic.getLevel();
    //map(incoming value to be converted, lower bound current range, upper bound current range, lower bound target value, upper bound target value);
    let mappedVolume = map(volume,0,1,0,500);
    
    /*Nested for-loop. gå gennem alle y'er i x = 0, efter gå gennem
    alle y'er i x=1 osv osv. Kan ramme alle x og y punkter i ens window */
    for (let y = 0; y < rows; y++) {
        let xoff = 0;
        for (let x = 0; x < cols; x++) {
            //index = (x + y * cols) = alle pixels på skærmen
            let index = x + y * cols;
            //Kunne være interessant at bruge lyd til at definere vinkel
            //Her der skal sættes ind i hvis det skal basere sig på lyd
            //let angle = mappedVolume * 6.28318 * 4
            //6.28318 = 2pi, og fortæller i dette tilfælde at vi vil have en vinkel fra 360 grader
            //4 gør at vectorene bliver mere forskellige   
            let angle = noise(xoff, yoff, zoff) * 6.28318 * mappedVolume;
            //Laver et vector fra en vinkel
            //Egentlig meget lig createVector (se Particles)
            let v = p5.Vector.fromAngle(angle);
            //Alle de ovenstående vectorer der beregnes, er opbevaret i dette array
            v.setMag(1);
            flowField[index] = v;
            xoff += inc;
            stroke(255, 50);
            strokeWeight(1);
            push();
            translate(x * scale, y * scale);
            //Roter ud fra vectorens vinkel
            rotate(v.heading());
            //line(0, 0, scale, 0);
            pop();
        }
        //Jo større inc (0.01 her), jo mere "noise" effekt
        yoff += inc;
        zoff += 0.0003;
    }
    //Ligesom vi har gjort med classes
    for (let i = 0; i < particles.length; i++) {
        //Follow, fortæller at hver partikel skal finde den vektor i arrayet der ligger tættest på sig
        particles[i].follow(flowField);
        particles[i].update();
        particles[i].edges();
        particles[i].show();
    }
}
