[Programmet](https://kevin.jensen.sletten.gitlab.io/aestetiskprogrammering/MiniX_7/minix7.html)

[Koden til programmet](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_7/minix7.js)

[Koden til classen](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_7/Particles.js)

Programmet skal have adgang til mikrofon, og bliver direkte påvirket af mikrofonens volume og mikrofon boost instillinger. Hvis programmet blot tegner mere eller mere vandrette streger kan det skyldes mikrofonen ikke opfanger lyde.

# Hvilken miniX har du valgt?
Jeg har valgt i denne minix7 at genbesøge min generative kode. Det er et af de emner der har fascineret mig mest, men samtidig også været et af de emner der har været sværest. Jeg er vildt fascineret af det levende/organiske i forhold til kodning. Vi har noget maskinelt der læser alt som binær kode, 1 og 0. Vi fodrer den med et ”læsbart” kodesprog, som basere sig på matematik og logik, og ved hjælpe af de relevante linjer, kan vi fremmane noget der simulere liv. Uden at gå ind i en filosofisk diskussion om hvad liv i det hele taget er for en størrelse, så er der, for mit vedkommende, et par essentielle punkter der skal opfyldes. Der skal være bevægelse, denne bevægelse skal være mere eller mindre ”flydende” og der skal være en grad af noget random, eller sagt på en anden måde, handlinger skal forekomme mere eller mindre uventet (perlin noise).
Disse selv samme punkter føler jeg står i kontrast til det mere maskinelle, det logiske, det planlagte, det forudsigelige, som langt hen ad vejen er computeren. 

Gennem arbejdet med min generative kode, og forsøget på at skabe noget der imitere simpelt liv, indeholdende de regler jeg selv lige har skrevet, slog det mig også hvor modsættende det er at jeg sidder og laver en class for at gøre koden overskuelig. Dette er nok meget sigende for hele science verdenen, sat på en spids, at alt kan sættes i kasser og dermed overskues, planlægges og kategoriseres, det er nok i virkeligheden meget sigende for hele menneskehedens tilgang til verden.


# Hvilke ændringer har du lavet og hvorfor?
I denne minix har jeg valgt at lave hele programmet om, så på den måde har jeg ikke genfortolket min tidligere minix, men i stedet en slags genstart så at sige. Den har enkle elementer der går igen, det være sig `perlin noise` og hele ideen om at lave et stykke der er i process frem for et færdigt stykke statisk kunst. Grunden til at jeg mente det var nødvendigt med en desideret genstart af godt og vel hele programmet, var fordi, at jeg som sagt følte dette emne var det sværeste, men samtidig også det mest fascinerende. Dette gjorde også at det var i denne opgave at jeg følte min forståelse for selve programmet var mangelfuld og visionen var haltende. 

# Hvad har du lært i denne miniX? Hvordan kunne du inkorporere eller videreudvikle koncepterne i din ReadMe/RunMe baseret på litteraturen vi har haft indtil nu?
Denne gang har jeg i langt højere grad forsøgt at inkorporere elementer fra tidligere, samt senere minix'er. Ikke kun i gennem koden, men i høj grad også i forhold til de reflektioner jeg har gjort mig før, under og efter. Denne gang har jeg implementeret en `class`. 
> "A class can therefore be understood as a template for, and blueprint of, things." s. 153
Altså at organisere min kode bedre og fordi jeg laver hundredevis af partikler og en `object class` gør det muligt at skabe et _blueprint_ med objektets udseende og bevægelse via dets `methods`, som i dette tilfælde består af:
```
update() - Der opdatere partiklernes hastighed, position og acceleration for hvert frame.
follow(vectors) - Der fortæller at partiklerne skal finde den vector der ligger tættest på i det én dimensionelle array, brug dens retning og brug den force. 
applyForce() - Akkumuler "force" til acceleration 
show() - Partiklernes udseende 
updatePrev() - Updatere partiklets seneste position til partiklets nuværende position.
edges() - Hvis partiklen kommer ud til kanten, sæt den over i modsatte side, baseret på dens tidligere placering (updatePrev). 
```  
Disse methods (funktioner), kaldes så i draw som:
```
for (let i = 0; i < particles.length; i++) {
    particles[i].follow(flowField);
    particles[i].update();
    particles[i].edges();
    particles[i].show();
```
Her går programmet igennem particles arrayet og tilføjer disse methods. Partiklerne tilføjes gennem:
```
for (let i = 0; i < 500; i++) {
    particles[i] = new Particles();
}
``` 
En anden ting jeg har gjort brug af denne gang, som jeg ikke benyttede i minix5 er et `nested for-loop`:
```
for (let y = 0; y < rows; y++) {
    for (let x = 0; x < cols; x++) {

```
Dette skaber det grid, som vectorene der er med til at drive det flowfield der skabes, sidder i.
I forhold til emnet datacapture, har jeg benyttet mig af optagelsen af lyd i denne minix, da jeg synes det giver en ekstra dimension til programmet. Denne lyd date bruges derefter til at have indflydelse på vectorenes bevægelser:
``` 
mic = new p5.AudioIn();
mic.start();

function draw(){
    let volume = mic.getLevel();
    let mappedVolume = map(volume,0,1,0,500);

    let angle = noise(xoff, yoff, zoff) * 6.28318 * mappedVolume;

    let v = p5.Vector.fromAngle(angle);
}

```
Det bryder den 4 væg og inkorporere enten en direkte lyd eller rummets ambiance ind i "værket". Dette aspekt snakkede vi kort om i Software Studies med Dr May Ee Wong, hvordan en by ikke er software, en af grundene der blev nævnt var at:
> "Environmental data are just as much figure as they are ground. They remind us of necessary truths: that urban intelligence comes in multiple forms, that it is produced within environmental as well as cultural contexts..." - Shannon Mattern, A City is Not a Computer.

Denne ambiance som programmet opfanger og gør til kunst er en del af det levede liv, en del af det miljømæssig såvel som kulturelle.
Soon & Cox referere til Jonathan Crary, og giver et lille uddrag fra _Capitalism and the ends of sleep_ hvor de skriver:
> "the collapse of the distinction between day and night, meaning we are destined to produce
data at all times." s. 115. 

Dette uddrag synes jeg også er interessant i forhold til min minix og datacapture, da vi igennem kurset har været meget kritiske, hvilket er en vigtig pointe, især når det kommer til udnyttelsen af førnævnte i forbindelse med kapitalisme og monetization, men det fik mig til at fokusere på at bruge datacapture, til at skabe noget smukt og inkluderende i stedet. Citatet gør sig også gældende i forhold til temporaliteten synes jeg. Vi skaber hele tiden og altid data, dette viser programmet også, med en mikrofon der er følsom nok, ville den også kunne skabe et stykke generativ kunst når du sov. Temporaliteten gør sig også gældende i denne mini x. Det er ikke fordi der er kodet tidsrelevante funktioner ind i programmet, men programmet er ikke et statisk billede, men et bevægende, løbende genererende og flydende billede. Et værk i "evig" proces. 
> "human beings’ knowledge of the end of their lives which inscribes a temporal sense of what it means to be a human...Humans live with the implicit awareness that their death is already future in the past.” Soon & Cox, s. 93.

Citatet har jeg ikke taget med fordi det beskriver det tidsaspekt der gør sig gældende i det at være menneske, men jeg finder det interessant i forhold til mit mål om at skabe et program der imitere liv. Liv er begrænset, programmet er "evigt". Maskintid kontra mennesketid. Dette citat understreger den menneskelige formodning om at alting har en ende, mennesketid er lineær, hvilket ikke nødvendigvis er gældende for maskinen. Ved at værket skabes for øjnene af og i samarbejde med tilskuer, involvere også en hel del tidslige forventninger. Hvad er det næste der sker? Hvordan ser det ud når det er færdigt? Denne igangværende proces bidrager til nysgerrighed og forventning. Bliver disse forventinger forløst, eller bliver man blot bevidst om denne evige proces som værket også illustrere, dette skel mellem mennesketid og maskintid?  

# Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere dit værk perspektiver i æstetisk programmeing (henvend evt. til forordet i grundbogen)?
For mig at se, gennem de sidste par måneder med med æstetisk programmering, er digital kultur noget vi forbruger, mens æstetisk programming omhandler i høj grad hvordan vi kan analysere og kritisk bidrage til denne digitale kultur. Digital kultur giver os en masse programmer/værktøjer hvorved vi kan tilgå hinanden eller miljøet omkring os. Disse værktøjer kan vi have den ene eller anden holdning til, og måske også umiddelbart se nogle inkluderende/eksluderende elementer på overfladen. Æstetisk programmering derimod, tager disse programmer/værktøjer og kigger dem i sømmene. Alle disse apps osv vi gør brug af, er skabt af mennesker, og indeholder dermed deres subjektivitet, fordomsfuldhed, og generelle verdensanskuelse, og dette kan ikke blot ses på overfladen, men gennem selve koden også. Hvad er inkluderet i koden, og hvilket er udeladt. 

Set på denne måde og ved at arbejde med programmering, bliver man bevidst om, at selv de mest livagtige programmer, som det jeg har forsøgt at skabe, er i bund og grund 0'er og 1 taller. Jeg har forsøgt at skabe noget med en grad af tilfældighed, ved hjælp af perlin noise, og selvom det virker "random" er jeg godt klar over at det er pseudo-random, der er rammer og begrænsninger.


<img src="leTigre-Deceptacon.gif" width="330" height="300">
<em>leTigre - Deceptacon</em>                                  
<img src="leTigre.PNG" width="300" height="300">
<em>leTigre - Deceptacon (done)</em>






<img src="Kaffekunst.PNG" width="350" height="300">
<em>KaffeKunst</em>


