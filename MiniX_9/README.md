# MiniX9 - Lineage of Thrones


[Programmet](https://kevin.jensen.sletten.gitlab.io/aestetiskprogrammering/MiniX_9/minix9.html)

[Koden til programmet](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_9/miniX9.js)

[Koden til Class](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_9/Text.js)

[JSON-filen](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_9/got.json)


## The weight of our fathers
[Buster](https://gitlab.com/bustersiem/aestetisk-programmering),[Mathias](https://gitlab.com/MathiasSund/aestetisk-programmering),[Christoffer](https://gitlab.com/Thundt/aestetiskprgrammering),[Kevin](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering)


Vores miniX er inspireret af opgavebeskrivelsens krav om at vise hvordan JSON virker på det tekniske niveau i forhold til hvordan en fil har egenskaberne til at opbevare specifikke data. Det fik os til at tænke, som de film og serie-“nørder” vi er, at det var oplagt at man kunne lave et stamtræ fra noget der er ‘in’ og noget som man bestemt kunne anse for at være pop-kulturelt; vi tænkte derfor at det kunne være fedt - og måske endda lidt hobby-agtigt - at lave en miniX om noget vi alle er fans af, så vi valgte at prøve at illustrerer stamtræerne i serien Game of Thrones. Vi tænkte på samme tid, som sagt, at der var rig mulighed for at vise JSON’s egenskaber på samme tid med, at vi lavede noget som vi alle sammen var interesserede i!

## Vores tanker om projektet


Vores fokus ligger på den inddeling, der er iboende og egentlig hovedformålet med en JSON-fil, men også inddeling i kasser i forhold til et stamtræ. Vores kode er i sin struktur afhængig af arrays, lister og inddeling af data, dette mener vi også er repræsenteret i et stamtræ. Soon & Cox beskriver hvordan kode ikke alene skal forstås på baggrund af dets funktionelle aspekt, men at kode afspejler hvordan vi gennem vores sprog udtrykker os selv og hvordan vi bliver forstået (Soon & Cox). På samme måde gør koden sig gældende i en sammeligning af med stamtræ i forhold til dets struktur, samt iboende hieraki. Programmets opbygning og noget så menneskeligt som menneskelig herkomst deler samme struktur. Når source code og programmet ses på denne måde, bliver kodens æstetik en direkte repræsentation, om end lidt kantet, noget de fleste af os kan nikke genkendende til i dets udformning og formål. På samme måde er kode opbygget i struktur og forbindelser - især i forhold til forbindelser, hvis man tænker på Object-Oriented Programming, men også generelt, at forskellige dele af kode påvirker hinanden. Hvis man tager funktioner som et eksempel, kan det siges, at en funktion i det globale scope også kan bruges som en condition i et if-statement i en helt anden funktion.
Vi har valgt at karakterne i stamtræet er mændene af ‘husene’ - så de er repræsenteret af faderens linje i familien. Hvis stamtræet når en hvis størrelse, tager man, også den dag i dag, udgangspunkt i faderens linje. Dette gøres for at skabe en rød tråd til historiens tidsperiode og det stadig eksisterende patriarkat i samfundet. Værket er en afspejling af hvordan samfundet stadig til den dag i dag til en hvis grad er domineret af mænd; som det har været i flere tusinder af år. Dette er også en måde hvorpå vi kan pointere et samfundsmæssigt problem (Soon & Cox), der tilsyneladende stadig gør sig gældende den dag i dag. 

## Programmet:


Kronologisk gennemgang af familiehusene i forhold til alder. Dette gøres for at repræsentere den værdisætning, der også sker mellem mænd og drenge; faderen er “vigtigere” for historien end sønnerne er. Soon og Cox pointere i teksten hvordan en af kernemetoderne i Vocable Code er regler (Soon & Cox), og i dette program har vi opsat regler, som at Faderens navn altid nævnes før børnene, hvilket illustrere at man, som søn står i skyggen af sin far, og generelt at mænd står i skyggen af familiens navn. Dette design mæssige valg både kodens struktur, men også i programmets visualisering er med til at stadfæste hvordan vi ønsker at programmet skal forståes (Soon & Cox).
Det er ikke længere et array, der i sit iboende hierarki bærer en værdi over mændene i en familie, men kan zoomes ud til et array der med sit hierarki afspejler værdien i selve samfundet.
JSON filen er meget struktureret, mens selve oplevelsen af programmet kan virke tilfældig og måske forstår man det ikke helt, ligesom en førstegangs seer måske har haft det. Soon & Cox pointere i teksten at high-level programming language ikke blot kommunikere instruktioner til maskninen, men kommunikere også mennesker imellem. Dette bliver meget åbenlyst i Soons egen Vocable Code hvor vi alle i gruppen og siddet og kæmpet for at forstå hvad hendes kode siger og hvad hun forsøger at sige (Soon & Cox). Dette gør sig ikke gældende i vores program føler vi. Ud over den er langt mere simpel, forsøger vi at kommentere hovedsageligt på kodes struktur og iboende hieraki og hvordan samme struktur og hieraki gør sig gældende i den virkelige verden, derfor mener vi også at vores kode i højere grad formår at kommunikere sin hensigt mennesker imellem, ved brug af symboler og populær kultur, så fremt man kender til Game of Thrones.

## JSON-filen


JSON filen består som sagt af diverse familier fra Game of Thrones-universet, hvor vi - ved hver familie - har lavet tre værdier (`colR`, `colG`, `colB`) for familiens våbenskjolds farver, og en array (`members`) til den faderlige linje af familien. Man kan godt sammenligne vores JSON-fil, men generelt bare JSON-filer, hvad de end indeholder, med et slags leksikon hvor man kan slå op og finde et givent stykke information eller data, som det jo egentlig er. Måden som vi i sketch.js-filen har tilgået denne array som er i en array, er ved at at lave en variable `data` i det globale scope som kobler sketch-filen sammen med JSON data-filen ved at `preload()` JSON-filen og dermed hente datasættet. For at komme i en den første array, som er `houses` laves der en variabel, som vi meget passende har kaldt for `houses`, der henter `data.houses` så programmet ved at den skal gå ind i `houses`-arrayet i JSON-filen. For at tilgå den næste array i JSON-filen - det vil sige `members` - laves der en ny variabel i sketch-filen, `members` (som bruger vores tidligere variabel `houses`, der indeholder vores datasæt): `let members = houses[i].members`.

## Sketch-filen

```
function deleteText() {
if (gradient < 0){
   names.splice(0);
   memberCounter++;
   gradient = 255;
   if (memberCounter >= data.houses[housePicker].members.length) {
      pickRandomHouse();
      memberCounter = 0;
   }
}
```
Vi har lavet `function deleteText` for at tjekke hvornår det første navn i familiernes stamtræ er usynligt på skærmen, så programmet ved at det nu er det næste navn i “rækken” (det vil sige i arrayet), der skal vises. I funktionen er der et if-statement der eksekverer hvis gradient variablen er ***mindre*** end 0 - hvilket er det tidspunkt hvorpå navnet er usynligt på kanvasset. Når if-statement så eksekverer skal der `.splice` fra `names`-arrayet, `memberCounter`-variablen skal **incrementes** med én og `gradient` sættes tilbage til 255 (som betyder at der er 100% **opacity** for navnene). I if-statementet er der endnu et if-statement der eksekverer når alle navne i en familie er blevet vist på kanvasset, hvilket programmet ved er når, at `memberCounter` er ***større end*** eller ***lig med*** længden på størrelsen af `members`-arrayet i JSON-filen, hvorefter der vælges en ny familie med `pickRandomHouse` og starter med det første navn i den nye familie med `memberCounter = 0`.
```
function pickRandomHouse() {
   housePicker = int(random(data.houses.length));
}
```
pickRandomHouse funktionen indeholder en variabel der hedder `housePicker`. I denne variabel går vi ind og først fortæller at det skal være en integer, altså et helt tal, derefter beder vi programmet vælge et random house fra vores JSON fil. Når dette så skal vise sig på skærmen benytter vi samme variabel i vores Class `Text.js` under funktionen `show()`:
```
    show() {
        textSize(this.size);
        gradient -= 2;
        fill(data.houses[housePicker].colR,data.houses[housePicker].colG,data.houses[housePicker].colB,gradient)
        text(data.houses[housePicker].members[memberCounter], this.x, this.y);
    }
```
For at få teksten til at vise sig ud fra den variabel beskrevet før, hvor et hus bliver valgt random, benytter vi `housePicker` i både **fill** og **text**. I **fill** bliver huset valgt, med dets tilhørende farve, som er defineret i JSON filen med `colR`, `colG` og `colB`, plus den tager alpha værdien efter den `gradient` variabel vi har lavet, om som er aftagende. I **text** vælges det random house ud fra `housePicker` og derefter går den gennem de `members` der tilhøre det specifikke house, med den tilhørende `memberCounter` variabel, der er lavet i MiniX9.js. På denne måde bestemmes det random house og vises på skærmen med de førnævnte funktioner og egenskaber, som den gradvise fading, `splice` og `memberCounter`. 

<img src="miniX9.gif" width="600" height="277">



