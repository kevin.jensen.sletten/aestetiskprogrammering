let data;
let names = []
let housePicker
let memberCounter = 0
let gradient = 255

function preload() {
    data = loadJSON("got.json");
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    textFont();
    stroke(150)
    strokeWeight(0.2)
    textAlign(CENTER);
    pickRandomHouse();
    
}

function draw() {
    
    background(0);

//pushes new text
    if (names.length == 0){
        names.push(new Text());
    }

    names[0].show();

    deleteText();


}

function pickRandomHouse(){
    housePicker = int(random(data.houses.length));
}



function deleteText() {
    if (gradient < 0){
        names.splice(0);
        memberCounter += 1
        gradient = 255
        if (memberCounter >= data.houses[housePicker].members.length){
            pickRandomHouse();
            memberCounter = 0
        };
    }
}



//pickRandomHouse does not get executed