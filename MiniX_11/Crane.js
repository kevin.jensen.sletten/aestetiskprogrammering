class Crane {
    constructor() {
      this.posX = width/2 + 150; // spiller starter i venstre side af skærmen
      this.posY = height / 2;
      this.moveFactor = 5; // bevægelseshastighed for skibet (spilleren)
      this.size = {w: 677 / 6, h: 774 / 6};
    }
   
   
    show() {
      
      image(craneOpen,
        this.posX,
        this.posY,
        this.size.w,
        this.size.h
        );
        
       /* rectMode(CENTER);
        fill(175);
        strokeWeight(2);
        rect(this.posX, this.posY, this.size);*/
    }
   
   
    moveUp() {
      this.posY -= this.moveFactor;
      // checker om vi er for højt oppe
      if (this.posY < 310) {
        this.posY = 310;
      }
    }
   
   
    moveDown() {
      this.posY += this.moveFactor;
      // checker om vi er for langt nede
      if (this.posY > 750) {
        this.posY = 750;
      }
    }
   }
   