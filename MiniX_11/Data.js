class Data{
    constructor() {
        this.posX = random(-75, -1250);
        this.posY = random(390, 635);
        this.speed = 5;
        this.size = {w: 328 / 6, h: 876 / 6};
        this.type = 1;
        //this.look = floor(random(0,3))
    }

    show(){
        image(figur1,
            this.posX,
            this.posY,
            this.size.w,
            this.size.h
            );

    }

    move(){
        this.posX += this.speed;
    }    
}
