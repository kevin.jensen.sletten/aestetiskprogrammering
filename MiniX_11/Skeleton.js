class Skeleton{
    constructor(_x, _y) {
        this.posX = _x;
        this.posY = _y;
        this.speed = 5;
        this.size = {w: 231 / 6, h: 839 / 6};
        this.type = 2;
    }

    show(){
        //fill(100);
        //ellipse(this.posX, this.posY, this.size);

        image(skeleton,
            this.posX,
            this.posY,
            this.size.w,
            this.size.h
            );
    }

    move(){
        this.posX += this.speed;
    }
}

