let cookies, startBackground, scareBackground, gameBackground, gameBackgroundOverlay, endBackground;
let figur1, figur2, figur3, skeleton, craneOpen;

let mode = 0;
let player;
let data = [];
let min_data = 7;
let missed = 0;
let score = 0;


function preload() {
    //Graphics
    startBackground = loadImage('assets/StartBackground.png');
    scareBackground = loadImage('assets/ScareBackground.png');
    gameBackground = loadImage('assets/GameBackground.png');
    gameBackgroundOverlay = loadImage('assets/GameBackgroundOverlay.png');
    endBackground = loadImage('assets/EndBackground.png');
    cookies = loadImage('assets/Cookies.png');
    figur1 = loadImage('assets/figur1.png');
    figur2 = loadImage('assets/figur2.png');
    figur3 = loadImage('assets/figur3.png');
    skeleton = loadImage('assets/Skeleton.png');
    craneOpen = loadImage('assets/CraneOpen.png');
    //craneClosed = loadImage('assets/CraneClosed.png');
    //Sound
    soundFormats('wav');
    startMusic = loadSound('assets/StartMusic.wav');
    goreSound = loadSound('assets/GoreSound.wav');
    gameMusic = loadSound('assets/GameMusic.wav');
    endMusic = loadSound('assets/EndMusic.wav');
}


function setup() {
    createCanvas(1920,1080);
    rectMode(CENTER);
    textAlign(CENTER);

    player = new Crane;

    for (let i = 0; i < min_data; i++) {
        data.push(new Data());
    }
}

function draw() {

    //Cookie skærm
    if (mode == 0) {
        image(startBackground, 0, 0, 1920, 970)
        //Button
        
        fill(0, 100)
        rect(width / 2, height / 2, 1920, 1080)
        image(cookies, 0, 350);
    }


    //Start skærm
    if (mode == 1) {
        image(startBackground, 0, 0, 1920, 970)
        //Button
        fill(255)
        rect(width / 2, height / 2, 256, 128)
        fill(0)
        rect(width / 2, height / 2, 248, 120)
        //Play text
        fill(255)
        textSize(40)
        text("PLAY", width / 2, height / 2 + 12)
    }
    //Scare skærm
    if (mode == 2) {
        image(scareBackground, 0, 0, 1920, 970)
    }
    //Spil delen
    if (mode == 3) {
        image(gameBackground, 0, 0, 1920, 970);
        player.show();

        checkDataNum();
        showData();
        image(gameBackgroundOverlay, 0, 0, 1920, 970);
        checkMissedData();
        checkCollectData();
        checkGameOver();
        checkScore();

        

        if (keyIsDown(UP_ARROW)) {
            player.moveUp();
        } else if (keyIsDown(DOWN_ARROW)) {
            player.moveDown();
        }

    }


    //Slut skærm
    if (mode == 4) {
        image(endBackground, 0, 0, 1920, 970)
        //Button
        fill(255)
        rect(1650, 875, 256, 108)
        fill(0)
        rect(1650, 875, 248, 100)
        //Play again text
        fill(255)
        textSize(50)
        text("You harvested " + (score * 43) + " dollars worth of data. Well done!", width / 2, 135)
        textSize(35)
        text("PLAY AGAIN", 1650, 875 + 12)
    }
}

function checkDataNum() {
    if (data.length < min_data) {
        data.push(new Data());
    }
}

function showData() {
    for (let i = 0; i < data.length; i++) {
        data[i].show();
        data[i].move();
    }
}

function checkMissedData() {
    for (let i = 0; i < data.length; i++) {
        
        if (data[i].posX > width + 55 && data[i].type == 1) {
            //if (data[i].posX > width + 55) {
            missed++;
            data.splice(i, 1);
            console.log("You missed some data. Missed data: " + missed + ".");
            console.log(i);
        }
        else if (data[i].posX > width + 55 && data[i].type == 2) { //Bruger else if fordi den ikke skal tjekke begge dele hvis den første er spliced (fixer vores bug med sidste element i array)
            data.splice(i, 1);
        }

    }

}

function checkCollectData() {
    let distance;

    for (let i = 0; i < data.length; i++) {
        distance = dist(
            player.posX,
            player.posY,
            data[i].posX,
            data[i].posY
        );

        //if (distance < player.size) {
        if (distance < player.size.w / 2 && data[i].type == 1) {
            data[i] = new Skeleton(data[i].posX, data[i].posY);
            //data.splice(i, 1);
            score++;
            console.log("You harvested " + score + " sets of data.");
            goreSound.play();


            //skeleton = new Skeleton;
            //skeleton.push(new Skeleton(data[i].posX, data[i].posY));
            //skeleton.show();
            /* Vi skal have lavet, så skeleton-pleceholderen bliver vist efter at man 'collector' et data-objekt
            skeleton.push(new Skeleton(player.posX, player.posY));
            */
        }
    }
}

function checkGameOver() {
    if (missed === 3) {
        mode = 4;
        gameMusic.stop();
        endMusic.loop();
        data.splice(data.lenght);
        console.log("You missed your chance to collect data.");
    }
}

function checkScore() {
    textAlign(CENTER);
    fill(255);
    stroke(10);
    textSize(30);
    text(score + " data sets harvested.", width / 2, 50);
}

function changeMode() {
    mode = 3;
}

//For at kunne klikke på start knappen og spil igen knap
function mouseClicked() {
    //Fake cookie til play
    if (mouseX < 1782 && mouseX > 1466 && mouseY > 812 && mouseY < 857 && mode == 0) {
        mode = 1;
        startMusic.loop();
    }


    //Gå fra play til scare til spil
    else if (mouseX < 1088 && mouseX > 832 && mouseY > 476 && mouseY < 604 && mode == 1) {
        mode = 2;
        startMusic.stop();
        goreSound.play();
        gameMusic.loop();
        setTimeout(changeMode, 2500);
    }


    //Gå fra end til spil
    else if (mouseX > 1520 && mouseX < 1775 && mouseY > 820 && mouseY < 925 && mode == 4) {
        mode = 3;
        endMusic.stop();
        gameMusic.loop();
        score = 0;
        missed = 0;
        player.posY = height / 2

        //Reset alle nødvendige ting KODES SENERE!!!
    }

}
