[Buster](https://gitlab.com/bustersiem/aestetisk-programmering),[Mathias](https://gitlab.com/MathiasSund/aestetisk-programmering),[Christoffer](https://gitlab.com/Thundt/aestetiskprgrammering),[Kevin](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering)

[Program](https://kevin.jensen.sletten.gitlab.io/aestetiskprogrammering/MiniX_11/index.html)

Scale & Layout skal være 100% i display settings ellers passer programmet ikke til skærmen. 

[Source Code](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_11/project.js) | 
[Skeleton Class](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_11/Skeleton.js) | 
[Data Class](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_11/Data.js) | 
[Crane Class](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_11/Crane.js) | 

<img src="data_farm.JPG" width="960" height="445">
