# Individuelle flowchart

Jeg har valgt at lave en individuel flowchart på min minix7, som er en reiteration af min minix5. Dette skyldes at jeg virkelig føler jeg gav alt jeg havde i mig og fik lavet det til den minix jeg er mest stolt af, men har samtidig også været den sværeste. Minix7 endte med at være den mest komplekse minix7 af dem alle, og inkorporere flere elementer og tanker af hvad vi har gennemgået i Æstetisk Programmering. Ved at lave et flowchart på denne minix, tvang jeg også mig selv til at forsøge at gøre flowchartet så forståeligt som muligt. Det endte ikke med at være så min mormor ville kunne forstå det, men føler alligevel, det fleste kan være med inklusiv mig selv. Min minix7 havde fokus på det levende, organiske og naturlige, og jeg har forsøgt at videreføre det i mit flowchart ved at skabe en form der i selve strukturen måske kunne minde om noget biologisk, næsten anatomisk, og de forbindende linjer er bløde og organiske.

<img src="Flowchartminix8.jpg" width="427,5" height="913,5">




# Gruppe flowchart

![Christoffer](https://gitlab.com/Thundt/aestetiskprgrammering), ![Buster](https://gitlab.com/bustersiem/aestetisk-programmering), ![Mathias](https://gitlab.com/MathiasSund/aestetisk-programmering), ![Kevin](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering)

<img src="Flowchart8gruppe.jpg">

**Første Brainstorm (OOP)**

I vores første brainstorm tog vi udgangspunkt i en af de største konflikter som foregår i øjeblikket, nemlig Rusland, Ukraine krigen. Vi føler det er et meget relevant emne, da det er den krig som har haft størst indflydelse på hvordan vi i gruppen lever, da det både er i Europa og foregår i vores levetid, og vi kan mærke det gennem f.eks. sanktioner som er del af verdens inflations problematik. Med vores program vil vi vise hvordan Rusland forsøger at overtage dele af Ukraines land, og Ukraine gør hvad de kan for at beskytte sig selv. 

Vi forestiller os at programmet fungerer ved at der er en masse små ukrainske flag som bevæger sig rundt på canvasset, mens et russisk flag bevæger sig rundt og opsluger de ukrainske flag og bliver større for hvert ukrainsk flag det opsluger. Til sidst vil det så ende med, at det russiske flag opsluger hele canvasset. Det at Rusland kun kan vinde skaber i sig selv en provokerende tankegang, og giver et billede af hvordan krigen kunne ende, hvis Ukraine ingen hjælp fik.


**Anden brainstorm (Generativ kode/Capture all)**

Affødt af den igangværende konflikt i Ukraine, befinder vi os i en ny kold krig, vest mod øst. Dette spreder tråde ud i vores dagligdag og blandt andet teknologi der før har været et kærkomment break fra hverdagen køres nu i stilling til at være et ‘weapons of mass surveillance' der aflyttes af fjenden. I dette scenarie er der flere forskellige faktorer der spiller ind. Det handler ikke alene om "fjenden", der aflytter os og indsamler data, men også om algoritmer, handlinger og italesættelser, der skaber splid og mistillid. Endvidere handler det om verdens økonomi, big data og corporate revenue. (Tiktok i europa og USA og whatsapp, discord og snapchat i Rusland og Kina)
Programmet visualiseret ved at canvas opdeles i hver sin farve, helst repræsentative for henholdsvis USA og Kina. Flag preloades for at gøre denne konflikt tydelig og let forståelig. Disse to farver, der samles af en gradient, bevæger sig vandret frem og tilbage over skærmen i en slags tovtrækning for at illustrere magtbalancen. Bevægelsen skabes af en perlin noise for at pointere denne magtbalance, som en flydende bevægelse. Når en af disse stormagter når den ene side af skærmen “vinder” nationen og programmet afsluttes. 


**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

I et flowchart vurderer og bestemmer vi hvad der inkluderes for bedst muligt at illustrere det vi mener er relevant viden for at brugeren kan danne sig det optimale billede af systemets kunnen og formåen. Dette betyder også at der sker en vurderingsproces i arbejdet med at lave et flowchart, da man simplificerer en forholdsvis kompleks og til tider lang kode. I denne vurderingsproces er det nemt at misse en afgørende syntaks, der ved udeblivelsen i flowchartet, kan skabe forvirring hos brugeren. På baggrund af dagens forelæsning, hvor Nathan Ensmenger inddrages, fremstår flowchart som en precursor for arbejdet med kodning. I den forstand baserer koden sig på flowchartet og simple vendinger illustrerer sjældent graden af kodens kompleksitet. Dette har vi opdaget kan skabe en smule problemer, da ambitioner ikke altid følges med kompetencer.  

**What are the technical challenges facing the two ideas and how are you going to address these?**

**IDÉ 1**

_Udfordring 1:_ Der ingen kontrol over hvornår canvasset et fyldt da flagene bevæger sig tilfældigt

_Løsning:_ Man kontrollerer hvor meget flaget skal vokse med pr. opslugt flag, men man kan styre hvor mange flag der skal opsluges før det russiske flag vokser.

_Udfordring 2:_ Der er en udfordring tematisk i vores program, da det kun er Rusland, der kan vinde, og derfor viser det et meget unuanceret billede af hvordan krigen egentlig foregår.

_Løsning:_ Man kunne lave nogle tilfældige tidspunkter i programmet hvor at Ruslands flag skrumper når det bliver ramt af et ukrainsk flag. Det ville give et tydeligere billede af, at krigen ikke er så entydig til Ruslands fordel.


**IDÉ 2**

_Udfordring 1:_ Et problem i idé 2 er at når perlin noise bruges, får man ingen 0 værdi i x-aksen som er afgørende for programmets funktion

_Løsning:_ Vi sænker grænsen for at vinde med -1 i hver side, så at hvis x værdien er på 1 vinder den ene side og modsat width -1 for den anden.

_Udfordring 2:_ Perlin noise er en pseudorandom funktion der ikke illustrere den virkelige magtbalance, som udenlandske digitale medier har indflydelse på

_Løsning:_ Man kunne lave en algoritme som søger nettet igennem for artikler der kommenterer for eller imod den ene eller den anden side, og derved bruge den data til at opnå et resultat.

**In which ways are the individual and the group flowcharts you produced useful?**

Vores gruppe flowcharts er brugbare til at konkretisere vores ideer, og få et tydeligt mentalt billede over hvordan programmet skal fungere. Det kan sammenlignes lidt med pseudo kodning på den måde, at man tænker over hvilke elementer og funktioner man vil inkludere i sin kode - det sætter tankerne i gang om man egentlig skal have med i koden og gør det nemmere når man rent faktisk skal kode. Det er værdifuldt i det at man kan artikulere sine ideer og tanker på en visuel måde i stedet for bare at beskrive dem med ord.

