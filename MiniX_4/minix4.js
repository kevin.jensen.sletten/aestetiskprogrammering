//Image arrays
let img = [];
let newImage = [];
//Variabler
let knap;
let capture;
let xDim = 800;
let yDim = 600;
let interval = 0;

//Funktion der kaldes som det første, for at få billederne med
function preload(){
    //Fortæller at programmet skal benytte web cam feed
    imgFeed = createCapture(VIDEO)
    //Fortæller programmet at det skal gemme feedet der ligger uden for canvas
    imgFeed.hide();
    
    //Mit image array, samt værdien fra web cam feedet, kaldt ovenfor
    img[0] = loadImage('../images/clothes.PNG');
    img[1] = loadImage('../images/food.PNG');
    img[2] = loadImage('../images/dog.PNG');
    img[3] = loadImage('../images/gadgets.PNG');
    img[4] = loadImage('../images/money.PNG');
    img[5] = imgFeed;
}

function setup(){
    /*Canvas størrelse ud fra predefineret variabler, troede det 
    skulle have en større rolle, men fik det ikke */
    createCanvas(xDim, yDim)
    //Webcam capture
    capture = createCapture(VIDEO);
    capture.size(xDim, yDim);
    capture.hide();

    //DOM for button inkl styles
    knap = createButton("Like");
    knap.position(width/2, height/2);
    knap.size(50, 50);
    knap.style("background-color", "#425260");
    knap.style("color", "#ffffff");
    //Når knappen trykkes på med musen, sker en defineret funktion
    knap.mousePressed(buttonPress);
}

function draw(){
    //Variabler for henholdvis lokation i y-aksen, samt bredden på billederne
    let y = 0;
    let b = 100;
    /*Variabel for millisekunder, samt værdi af let interval, som er kaldt tidligere
    'int' betyder at min return value skal være i hele tal, og det lille regnestykke
    er millisekunder til sekunder, så altså 1 sekund er mit tidsinterval*/
    let m = millis();
    interval = int(m / 1000);
    
    //Bruger web cam feeded på canvas
    image(capture, 0, 0, 800, 600);
    
    /*Hvis intervalet (antal sekunder) er større end 54 (max antal billeder) 
    så stopper den med at producere flere billeder, minus det sidste billde
    i arrayet*/
    if(interval > 8 * 6 - 1){
        interval = 8 * 6 - 1;
    }

    /*Putter billedet ind i det nye array på forgående plads, dette gøres for
    at det undgå at det nye billede ved mousePressed udskifter alle billeder
    i det gamle array, men skiftes med et enkelt billede */
    newImage[interval] = img[interval % img.length];
    
    //Får max 8 billeder til at vise sig på skærmen langs y-aksen, for hvert sekund
    for(let i = 0; i <= interval; i++){
        y = int(i/8);
        image(newImage[i], i % 8 * 100, y*100, b, b); // (variable for the image, x, y, w, h)
    }
}

//Funktion der fortæller hvad der skal ske når knappen trykkes
function buttonPress(){
    //Når knappen trykkes indsættes et live feed oven på det forgående billede
    newImage[interval - 1] = imgFeed;
}
