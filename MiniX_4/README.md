# minix4 ReadME

## Contemporary artpiece by Kevin Jensen
### Titled What you "Like" is what you get

<img src="minix4screenshot.PNG" width="400" height="300">


This minor [piece](https://kevin.jensen.sletten.gitlab.io/aestetiskprogrammering/MiniX_4/minix4.html) is inspired by Soon & Cox' chapter on "Metrics of likes". In this piece I try to comment on the implication of the algorithm of likes. That the standardization of out experiences when we venture through the internet in the end, acts like a contraint of our own making, and acts as limitations for our cultural curiosity. The things that you like is the things your gonna see more of. It's a dobbelt edged sword. You like a thing because it catches your interests, or speeks to something in you in a positive way. Maybe it you get even more curious and interested through even more similar posts and pushes you to follow a certain goal because it inspires you. In the same way can it act as a barrier, now you only see similar posts, and your getting limited, you only see one angle, or one answer to the questions, and your not exposed to contradicting point of views.


This small program tries to comment on exactly this. Everytime you press like, you get more of the same, and loose alternatives. The web cam feed as a canvas acts as a mirror, hopefully encouraging self-reflection. The mini web cam feeds that appear everytime you press like, serves to illustrate the more self-centred and narrowing path you are walking.


`inb4`teksten ovenover skal tages med et gran salt, hovedpointen omkring likes og algoritmer er god nok, resten er vidst bare lidt kunstnerisk fylde.

![](https://media.giphy.com/media/47zXx8V8Y7amj7AO7m/giphy.gif)

## Beskriv dit program, hvad du har brugt og lært
Som nævnt ovenover har jeg forsøgt at skabe et program der skal illustrere likes algoritmer og dets betydning for brugeren.
Jeg har benyttet mig af `arrays []`for at lave en beholder til alle de billeder jeg gerne ville benytte til at illustrere forskellige "emner". Jeg har lavet to arrays med de samme billeder i, fordi jeg havde lidt problemer med at når jeg trykkede "like" ved et billede af hunden f.eks. så byttede den alle billeder af hunden ud med webcam feeded, da den erstattede billedet i arrayet med webcam feeded. Ved at skabe et nyt array, kunne jeg i stedet bytte hvert enkelt billede med web cam feeded. 

Som i sidste minix benytter jeg mig denne gang igen af millisekunder og sekunder ved 
```
let interval = 0;
let m = millis();

interval = int(m / 1000);
```
Dette gør jeg fordi jeg føler det er en rigtig god måde at få noget kontrol ind i koden. Ved at benytte mig af sekunder, kan jeg nemmere styre de forskellige elementer i koden og hastigheden.


Denne gang lært jeg at lave en knap:
```
knap = createButton("like");
knap.position(width/2, height/2);
knap.size (50, 50);
knap.style("background-color", "#425260");
etc
```
Gøre brug af webcam:
```
createCapture(VIDEO);
capture.size();
capture.hide();

image(createCapture, 0, 0, 800, 600);
```
og eksperimentere lidt mere med arrays og for loops, som stadig er vildt svært synes jeg. Jeg havde også lidt bøvl med at style knappen helt, som jeg ville, om det var fordi det ikke kunne lade sig gøre i p5 eller det i stedet skulle gøre i html filen er jeg ikke sikker på. 

[Source code](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_4/minix4.js)


## Hvordan vedrører dine tanker og selve programmet emnet "Capture all" og hvilke kulturelle implikationer indebærer det?
Jeg har med min opgave kun berørt et lille område af hele emnet omkring datacapture. Grunden til jeg valgte at fokusere meget direkte på like kultur og algoritmer der vedrører dette, skyldes at det nok er det spiller den største rolle i hverdagen og er en del mere håndgribeligt end mange af de andre områder hvor datacapture indgår.

Så snart du tænder et elektronisk objekt med adgang til nettet, så tænker jeg at ens data bliver lagret et eller andet sted. Ved nærmere eftertanke, så selv hvis objektet **ikke** har direkte adgang til nettet. Datafication er et ret uhyggeligt emne, synes jeg, at vores liv gøres op i information, som der er nogen man aldrig har mødt, eller overhovedet ved hvem er, tjener penge på, over desuden er der så igen nogen man ikke ved hvem er der køber denne information om en. 

Det virker måske lidt ligegyldigt at, "nåeh ja, så er der nogen i USA der ved at du søger konsekvent på billeder af katte når du er på toilet", men som Shoshana Zuboff pointere så er dette en form for capitalistisk overvågning. Igen er der måske mange der har den holdning at overvågning er jo ikke dårligt når man er en dydig borger og opfører sig ordentligt, i det virkelige såvel som i ens online liv. Problemet er at man ikke ved hvem der definere hvad der er ordentligt. 

Jeg kom vidst ud på et sidespor men alt i alt, vedrører min kode blot en meget lille del af hele datacapture feltet, dog stadig et vigtigt område, mener jeg. 

![](https://media.giphy.com/media/xUOxffdzmmFj3ua0HC/giphy.gif)
