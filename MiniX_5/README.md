# Generativ kode

## Hvilke regler har du sat for dit generative program?

[RunMe](https://kevin.jensen.sletten.gitlab.io/aestetiskprogrammering/MiniX_5/minix5.html)

[Source Code](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_5/minix5.js)

Jeg satte mig selv de regler at når linjen, som mit program laver, når ned i bunden af canvas, så skal der dannes en ny linje. En anden regel skulle være at når en ny linje blev dannet, skulle den have en ny farve. Dette formåede jeg at løse ved brug af koden:

```
    line(x, 0, xEnd, yEnd);
    if (yEnd >= height) {
        position = 0;
        x += 2;
        stroke(random(255), random(255), random(255));
    }
    position++;
```
Så når linjens slut position bliver større eller lig med "højden" på canvas, rykker den sig 2 i x-aksen, skifter farve og starter en ny linje.

## Hvilken rolle har regler og processor i dit program?

De regler jeg har sat mig selv i programmet, har til dels været for at skubbe programmet fremad. Reglerne er med til at give programmet en lidt dybere dimension (næsten en slags 3D-effekt) i og med at farven skaber dybde i det den ligges lag på lag, på samme måde som linjerne ligges lag på lag, og skaber en nærmst vifte mønster ved hjælp af:
```
    if (position < end) {

        xEnd = (xEnd / end) * position;
        yEnd = (yEnd / end) * position;
    }
```
Desuden har jeg gjort brug af perlin noise i forhold til at skabe en mere "levende" kode. Denne noise værdi spiller ind på hele linjens færden i sin rejse ned af y-aksen. Ikke nok med at linjen nærmest bliver "levende" den resultere også i at hver enkelt linje til slut har fået skabt sig et bølgende og organisk mønster, som igen bidrager til denne 3D-effekt.

Jeg valgte at udkommentere for/while loopende da de får programmet til at lave alle mellemregninger "bag scenen" og viser kun det endelige kunstværk, men personligt synes jeg det er fedest at følge med i den "random" proces der sker i det kunstværket fremstilles. Ved at udkommentere for loopet lader jeg også "kunstværket" emergere gradvist, værket er fremstilles dermed mens det beskues, og man står måske og danner sig forestillinger om hvordan det ender, og regner ud hvilken kode der skaber processen i baggrunden. Disse refleksion ville ikke gøre sig gældende på samme måde, hvis billedet var færdigt når programmet startes.

## I forhold til det der skulle læses, hvordan hjælper denne miniX med at forstå ideen omkring "auto-generator"?

Motivationen i forhold til denne emne startede med at være tårnhøjt, men blev hurtigt knust af ambitioner der ikke hang sammen med kompetencer. Til sidst endte jeg med at finde MEGET konkret inspiration på nettet, som jeg ændrede en del på. Jeg er vil med ideen og emnet. Der er noget fedt i at få noget dødt mekanisk (computer, software) til at skabe en simulation af noget levende, det gør sig gældende i langstons ant og Daniels Shiffmans videor om "The nature of code". Mine kompetencer rakte desværre ikke til at få mit program til at simulere liv, men føler alligevel jeg opnåede en illusion af noget der ligger ud over egen indflydelse. At det program der kører og det "kunstværk" det ende ud med, har en følelse af at ligge uden for vores hænder, og 100% styret af maskinen. 


Jeg synes det er svært at lave en direkte referance til teksten da det jeg har lavet og de eksempler der vises i teksten er meget forskellige føler jeg. 10print er nok det min egen kode minder mest om, i det de begge bærer præg af at være descideret computer "kunst". De er fremstår og er i princippet, ret simple. De er ikke som langstons ant en simulation af liv, de agere ikke levende, selvom jeg godt ved jeg kaldte koden "levende" før, men igen, det er tydeligt det er computer "kunst". 

At koden køres, mellemregningerne laves og gemmes væk og det færdige "kunstværk" fremstår så snart programmet startes, tager for mit vedkommende hele den generative process fra værket. Det er selvfølgelig iboende i værket, ved at det er et nyt billede, hver gang siden reloades, men fornemmelsen er en anden, nysgerrigheden, tanken om det tilfældige og muligvis refleksionerne forsvinder ud af værket. 

Det interessante i eksemplerne fra bogen er for mit vedkommende ikke det færdige resultat, det er processen. Hvor ender det? Hvordan er det lavet? Hvilket mønster laver det nu? 10Print laver flotte mønstre der fører tankerne hen på labyrinter, ved hjælp af to simple tegn "/" "\", og det er forskellige hvad den laver hver gang. Langstons ant giver en fornemmelsen af noget levende og organisk, selvom det bare er en pixel på en skærm, og desuden forsvinder magiens for mit vedkommende en smule, så snart man indser at det er det samme mønster den laver hver gang, og den er ikke "levende". 


<img src="generativkode.PNG" width="400" height="300">
