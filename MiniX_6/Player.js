class Player {
    constructor() {
        this.pos = new createVector(width/2, height/2);
        this.speed = 10;
        this.size = {w: 50, h: 50}; 
    }
    
    moveLeft() {
        this.pos.x -= this.speed;
    }
    
    moveRight() {
        this.pos.x += this.speed;
    }

    moveUp(){
        this.pos.y -= this.speed;
    }

    moveDown(){
        this.pos.y += this.speed;
    }

    show() {
        /*fill(0);
        noStroke();
        rectMode(CENTER);
        rect(this.pos.x, this.pos.y, this.size);*/
        imageMode(CENTER);
        image(playerImg, this.pos.x, this.pos.y, this.size.w, this.size.h);

        if (player.pos.y > height) {
            player.pos.y = height;
        } else if (player.pos.y < 0) {
            player.pos.y = 0;
        }
        if (player.pos.x < 0) {
            player.pos.x = 0;
        } else if (player.pos.x > width) {
            player.pos.x = width;
        }
    }
}
    