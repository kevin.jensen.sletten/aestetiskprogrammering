//You work you lose the game

/* 
Spillet skal kommentere på vores arbejdskultur og selv medicinering.
Boosts i spil er altid symboliseret ved noget ud over spilleren selv,
altså et object man typisk indtager eller absorbere ved at løbe ind i det.
Dette kan perspektiveres til nogle nationers misbrug af opioider eller
midler som ritalin. En større og større samfunds sygdom er samtidig stres.
*/

//Player class
//Objects classes

//Must have:
//Player skal løbe rundt i både x og y aksen (✔)
//Player skal score point ved at "fange" arbejdsopgaver (✔)
//Player kan fange "medecin" der gør en hurtigere (✔)
//Player "dør" hvis spiller ikke "fanger" arbejdsopgaver (✔)
//Player "dør" efter spiller har "fanget" x antal arbejdsopgaver (✔)
//Instant replay, spillet stopper aldrig, du er "fanget" i hamsterhjulet (✔)
//De forskellige objekter er forskellige point værd (✔)

//Nice to have:
//Player "dør" hvis spiller ikke er i bevægelse i et bestemt antal sekunder
//Medicin forekommer efter x antal sekunder i stedet for min_medicin
/*Gradvis flere og flere jobs, måske med "lommer" af stilstand, hvor spilleren 
stadig skal holde sig i bevægelse for ikke at tabe, efterfulgt af en "kæmpe flod" 
af jobs der kommer ind over skærmen (evt rediger max antal point)*/
//Tacky inspirational quotes efter x antal point
//Bonus i form af coins på skærmen
//Flot interface
//Tekst bokse
//Intro skærm
//Musik


let kontor = [];
let fabrik = [];
let medicin = [];
let min_kontor = 3;
let min_fabrik = 5;
let min_medicin = 1;
let player;
let antal = 0;
let point = 0;
let tekstFarve = 40; 
let missed = 0;
let fabrikImg;
let kontorImg;
let medicinImg;
let playerImg;

function preload() {
    fabrikImg = loadImage('fabrik.png');
    kontorImg = loadImage('kontor.png');
    medicinImg = loadImage('medicin.png');
    playerImg = loadImage('player.png');
}

function setup() {
    createCanvas(800, 600);
    //frameRate(24);
    background(200);
    player = new Player();
    
}

function draw() {
    background(200);

    //Ville være fe' hvis de virkede
    //setInterval(showMedicin(), 5000);

    player.show();
    checkKontorNum();
    showKontor();
    checkFabrikNum();
    showFabrik();
    checkMedicinNum();
    showMedicin();
    checkCollision();
    checkGone();
    displayScore();
    checkResultat();

/*Bevægelse af player, giver flydende bevægelse i alle retninger, 
tager værdier fra player class */
    if (keyIsDown(LEFT_ARROW)) {
        player.moveLeft();
    } if (keyIsDown(RIGHT_ARROW)) {
        player.moveRight();
    } if (keyIsDown(UP_ARROW)) {
        player.moveUp();
    } if (keyIsDown(DOWN_ARROW)) {
        player.moveDown();
    }
}
/*Checker hvor mange kontor der er i spil, hvis den bliver mindre end
min_kontor, som er 3, kalder den på flere, så det svare til 3*/
function checkKontorNum() {
    if (kontor.length < min_kontor) {
        kontor.push(new Kontor());
    }
}
/*function der hjælper med at kalde de methods der er lavet i Class
i dette tilfælde dens bevægelse og hvordan den ser ud*/
function showKontor() {
    for (let i = 0; i < kontor.length; i++) {
        kontor[i].move();
        kontor[i].show();
    }
}

function checkFabrikNum() {
    if (fabrik.length < min_fabrik) {
        fabrik.push(new Fabrik());
    }
}

function showFabrik() {
    for (let i = 0; i < fabrik.length; i++) {
        fabrik[i].move();
        fabrik[i].show();
    }
}

/*function showMedicin() {
    fill(0, 0, 255);
    rectMode(CENTER);
    rect(100, 100, 60, 60);
    
}*/

function checkMedicinNum() {
    if (medicin.length < min_medicin) {
        medicin.push(new Medicin());
    }
}

function showMedicin() {
    //console.log("MEDICIN NAMNAM");
    for (let i = 0; i < medicin.length; i++) {
        medicin[i].move();
        medicin[i].show();
    }
}
/*Checker hvorvidt de forskellige objekter er på banen, hvis de går
ud over banens bredde (-60px) splice (sletter) den objektet */
function checkGone() {
    for (let i = 0; i < fabrik.length; i++) {
        if (fabrik[i].pos.x < -60) {
            fabrik.splice(i, 1);
            missed++
            //console.log("ramt fabrik");
        }
    }
    for (let i = 0; i < kontor.length; i++) {
        if (kontor[i].pos.x < -60) {
            kontor.splice(i, 1);
            missed++
            //console.log("ramt kontor");
        }
    }
    for (let i = 0; i < medicin.length; i++) {
        if (medicin[i].pos.x < -60) {
            medicin.splice(i, 1);
            //console.log("ramt medicin");
        }
    }
}
/*Checker om hvorvidt distancen mellem player og objektet rammer hinanden, 
hvis dette er tilfældet splices (slettes) objektet og et ny pushes (dannes) */
function checkCollision() {
    let distance;

    for (let i = 0; i < fabrik.length; i++) {
        distance = dist(player.pos.x, player.pos.y, 
            fabrik[i].pos.x, fabrik[i].pos.y);
        
        if (distance < player.size.w + 15) {
            fabrik.splice(i, 1);
            point++ //betyder at man ligger 1 til variablen point
            fabrik.push(new Fabrik());
        }
    }
    for (let i = 0; i < kontor.length; i++) {
        distance = dist(player.pos.x, player.pos.y, 
            kontor[i].pos.x, kontor[i].pos.y);

        if (distance < player.size.w + 15) {
            kontor.splice(i, 1);
            point += 2
            kontor.push(new Kontor());
        }
    }
    for (let i = 0; i < medicin.length; i++) {
        distance = dist(player.pos.x, player.pos.y, 
            medicin[i].pos.x, medicin[i].pos.y);

        if (distance < player.size.w + 15) {
            medicin.splice(i, 1);
            medicin.push(new Medicin());
            player.speed += 1.2
        }
    }
}

function displayScore() {
    fill(tekstFarve, 160);
    textSize(17);
    textAlign(LEFT);
    text('You have reached ' + point + " job(s)", 10, height - 60);
    text('You have missed ' + missed + " job(s)", 10, height - 40);
    fill(tekstFarve, 255);
    text('Press arrow keys to control your worker', 10, height - 20);
}

function checkResultat() {
    if (missed > point && missed > 2) {
        fill(tekstFarve, 255);
        textSize(26);
        textAlign(CENTER);
        text("YOU ARE FIRED!", width / 2, height / 2);
        noLoop();
        //efter spillet er tabt, 5 sekunders pause, og så resetSpil funktion
        setTimeout(resetSpil, 5000);
    }
    if (point > 100) {
        fill(255, 0, 0);
        rectMode(CENTER);
        rect(width/2, height/2, 800, 600)
        
        fill(tekstFarve, 255);
        textSize(26);
        textAlign(CENTER);
        text("You overworked yourself and died...", width / 2, height / 2);
        noLoop();
        setTimeout(resetSpil, 5000);
    }
}

//funktion der får spillet til at starte forfra
function resetSpil() {
    point = 0;
    missed = 0;
    player.speed = 10;
    kontor.splice(0, kontor.length);
    fabrik.splice(0, fabrik.length);
    medicin.splice(0, medicin.length);
    //Nulstiller alt først, og kører efterfølgende loopet igen
    loop();
}
