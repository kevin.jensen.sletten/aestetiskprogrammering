

class Kontor {
    constructor() {
        this.speed = (random(3, 10));
        this.size = {w: 60, h: 50}//floor(random(30, 70));
        this.pos = new createVector(random(width + 30, width + 100), random(height));
    }
    
    move() {
        this.pos.x -= this.speed;
    }

    show() {
        /*fill(0, 0, 255);
        noStroke();
        ellipseMode(CENTER);
        ellipse(this.pos.x, this.pos.y, this.size);*/
        imageMode(CENTER);
        image(kontorImg, this.pos.x, this.pos.y, this.size.w, this.size.h);
    }
}

class Fabrik {
    constructor() {
        this.speed = (random(1, 4));
        this.size = {w: 80, h: 60};//(random(50, 90));
        this.pos = new createVector(random(width + 10, width + 30), random(height));
    }

    move() {
        this.pos.x -= this.speed;
    }

    show() {
        /*fill(255, 0, 0);
        noStroke();
        ellipseMode(CENTER);
        ellipse(this.pos.x, this.pos.y, this.size);*/
        imageMode(CENTER);
        image(fabrikImg, this.pos.x, this.pos.y, this.size.w, this.size.h);
    }
}

class Medicin {
    constructor() {
        this.speed = (random(5, 10));
        this.size = {w: 30, h: 30};
        this.pos = new createVector(random(width + 10, width + 16), random(height));
    }

    move() {
        this.pos.x -= this.speed;
    }

    show() {
        /*fill(0, 255, 0);
        noStroke();
        ellipseMode(CENTER);
        ellipse(this.pos.x, this.pos.y, this.size);*/
        imageMode(CENTER);
        image(medicinImg, this.pos.x, this.pos.y, this.size.w, this.size.h);
    }
}

