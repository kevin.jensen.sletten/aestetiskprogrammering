# minix6 Games with objects

<img src="screenshot.png" width="400" height="300">

[Objekt class](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_6/Objects.js)

[Player class](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_6/Player.js)

[Spil kode](https://gitlab.com/kevin.jensen.sletten/aestetiskprogrammering/-/blob/main/MiniX_6/workGame.js)

## Mit program

Jeg har lavet spillet [**You Work You Lose - The Game**](https://kevin.jensen.sletten.gitlab.io/aestetiskprogrammering/MiniX_6/minix6.html). I alt sin relative simpelhed handler spillet om at "fange" alle de objekter der kommer flyvende hen af skærmen. For hvert objekt man fanger får man point, og hvis man misser flere objekter end man fanger så taber man. For at hjælpe spilleren kommer der _boosts_ flyvende hen over skærmen også, når disse fanges af spilleren, øges spillerens hastighed.

I udførelsen af mit program, har jeg taget inspiration i mange af de funktioner, som vi lærte i den foregående uges undervisning, gennem spillende ToFuGo og Tomatogame. Der er mange af de samme funktioner i spil, men hvor jeg har forsøgt at bygge videre og give flere funktioner, samtidig med at jeg selvfølgelig har forsøgt at give funktionerne og udseendet egenskaber, der passer bedst muligt med det udtryk og det budskab, jeg forsøger at formidle i spillet.

Mit spil består af fire forskellige ***classes*** fordelt på to forskellige ***.js*** filer. Min ***Obejcts.js*** fil indeholder henholvis en `Kontor`-class, `Fabrik`-class og en `Medicin`-class. Derudover har jeg en ***Player.js***, som indeholder min `Player`-class. Dette har jeg gjort i et forsøg på at skabe et overblik over kontrollerbare objekter `Player` og automatiske objekter, desuden bliver de kaldt forskelligt i "hoved" .js filen.

De automatiske objekter sættes i et tomt array, `kontor = [];`, som agere beholder for de gentagne ***kontor*** objekter der kommer til at fremgå i spillet. Det giver samtidig en del funktioner i forhold til `splice` og `push`, som henholdvis fjerner og tilføjer instances til arrayet. For at gøre brug af de forskellige ***classes*** og få dem til at vise sig i spillet, har jeg skabt to funktioner, den første:
```
function showKontor() {
        for (let i = 0; i < kontor.length; i++) {
        kontor[i].move();
        kontor[i].show();
}
```
Der looper de "iboende" `methods` ***move()*** og ***show()*** der er defineret i ***classen***. Disse funktioner indeholder kode der beskriver objektets bevægelse og objektets udseende, og `for-loop`et putter det ind i mit tomme array og viser det på på mit canvas. Dette er gjort ved hver enkel af de "automatiske" ***classes***. 
Den anden funktion der relatere sig til objekter er:
```
function checkKontorNum() {
    if (kontor.length < min_kontor) {
        kontor.push(new Kontor());
    }
}
```
Denne funktion Checker hvor mange kontor der er i spil fra arrayet, hvis den bliver mindre end `min_kontor`, som er 3, kalder den på flere, så det svare til 3.

For at give spilleren evnen til at "fange" disse objekter, er det nødvendigt at gøre ***player*** objektet "bevidst" om de automatiske objekter. Dette kan ikke gøre, som en game engine, hvor objektet kan få en collision kasse omkring sig, så det skal gøres mere manuelt ved hjælp af:
```
function checkCollision() {
    let distance;
    for (let i = 0; i < kontor.length; i++) {
        distance = dist(player.pos.x, player.pos.y, 
            kontor[i].pos.x, kontor[i].pos.y);

        if (distance < player.size + 15) {
            kontor.splice(i, 1);
            point++
            kontor.push(new Kontor());
```
Her checker programmet om hvorvidt distancen mellem ***player.position.på.x-aksen*** og ***player.position.på.y-aksen*** og objektets position på x- og y-aksen rammer hinanden. I `if statementet` siger den, hvis dette er tilfældet `splices` (slettes) objektet, et ny `pushes` (dannes) fra Kontor ***classen***, desuden får spilleren et point.

Hvis spilleren ikke fanger objektet, er der en `if statement` der checker om objektet ryger ud af canvas:
```
    for (let i = 0; i < kontor.length; i++) {
        if (kontor[i].pos.x < -60) {
            kontor.splice(i, 1);
            missed++
```
Hvis objektet ryger ud af canvas `splices` det og den tæller den som ***missed***, som gør sig gældende i spillet ved at være en ***condition*** i linjen `if (missed > point && missed > 2) {}`. 

## Mine refleksioner

Første havde jeg sat mit ambitionsniveau lidt for højt, da det at programmere/lave et spil er en kæmpe motivationsfaktor i dette kursus. Dog levede mine kompetencer ikke op til mit ambitionsniveau, og jeg fandt ud af at jeg ikke havde noget budskab med spillet, det illustrerede ikke det Soon & Cox pointere i bogen:
>(...) this object-oriented modeling of the world is a social-technical practice, “compressing and abstracting relations operative at different scales of reality, composing new forms of agency.” In their opinion, this agency operates across the computational and material arrangement of everyday encounters. (s. 161)

Det spil jeg har formået at skabe efter at jeg startede forfra, startede i højere grad med et budskab, eller i hvert fald nogle tanker jeg har gjort mig, og havde langt større fokus på udtryk frem for gameplay og udseende.

Det spil jeg er endt med at lave skal karikeret give udtryk for et, hvad jeg personligt mener, er et sygt abejdsmiljø. Du, som spiller, render rundt og det handler om at nå så meget, som muligt, så hurtigt, som muligt. Hvis du misser mere end du fanger, i dette tilfælde arbejde, så taber du og spillet er slut. Dette skaber også en mindre abstration i forhold til at du kan fange/arbejde så meget du vil, og det er "godt", men vægter du en smule mere til den anden side, så taber du. Dette er selvfølgelig sat på en spids, og skal tages med et gran salt.
Lidt i forhold til:
>OOP is designed to reflect the way the world is organized and imagined, at least from the computer programmers’ perspective.

Desuden har jeg implementeret et _boost_ i spillet, hvilket på ingen måde er unormalt i et spil, men det slog mig i mine refleksioner, at disse _boosts_ er typisk noget en spillers figur indtager/absorbere. Dette forbandt jeg med den epidemi af opioide misbrug der finder sted rundt omkring, og ulovlig salg af medicin som Ritalin, alt sammen for at kunne følge med, og holde ud i en hektisk og evigt krævende hverdag. Dette er også grunden til at mine _boosts_ i spillet er formet som medicin og når disse fanges af spilleren, bliver spilleren hurtigere. 

Spillet er også bevidst lavet så man ikke kan vinde det. Når du har fået point nok taber du spillet alligevel, i spillets kontekst, dør du af overarbejde. I samme tråd, starter spillet også bare forfra uden at du skal trykke noget, for at symbolisere at det er svært at gøre andet end at spille spillet, og at når hamsterhjulet først drejer, så sidder du fast i et loop.

Der er også forskellige point i spille. Fabrikken giver 1 point `point++`, men kontor giver 2 point `point+= 2`. Dette har jeg gjort for at illustrere den samfundsmæssige symbolske værdi der ligger i forskellige slags jobs, hvor noget synes at være bedre, eller mere værd end andre jobs.

Mit formål med spillet er at kommentere på vores abejdskultur sat på en spids, overdrivelse fremmer forståelsen. Jeg har også forsøgt i forhold til de mange forskellige elementer der flyver rundt på skærmen, at gøre spillet hektisk og kaotisk, og forhåbentlig sidder man, som spiller, tilsidst og har en tom følelse indeni.

Assets af Anna Lønborg
